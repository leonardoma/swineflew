﻿using UnityEngine;
using System.Collections;

public class ending_cutscene_music : MonoBehaviour {

	public float startTime;
	public float time;

	int counter;

	KeyCode jump_joystick, split_joystick, join_joystick, stick_joystick;

	// Use this for initialization
	void Start () {

		this.GetComponent<AudioSource>().time = startTime;

		if (Input.GetJoystickNames ().Length > 0)
		{
			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				split_joystick = KeyCode.JoystickButton12;
				join_joystick = KeyCode.JoystickButton13;
				stick_joystick = KeyCode.JoystickButton9;
				
				print ("ps3 controller connected");
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				split_joystick = KeyCode.JoystickButton3;
				join_joystick = KeyCode.JoystickButton2;
				stick_joystick = KeyCode.JoystickButton7;
				
				print ("ps4 controller connected");
			}
		}
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(jump_joystick))
		{
			counter ++;
		}

		time = 	this.GetComponent<AudioSource>().time;

		if (time > 37f || counter > 2)
		{
			Application.LoadLevel(0);
		}
	
	}
}
