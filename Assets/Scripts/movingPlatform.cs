﻿using UnityEngine;
using System.Collections;

public class movingPlatform : MonoBehaviour {
    public float moveSpeed;
    public float moveDistanceX;
    public float moveDistanceY;

    private float timer;
    private float distanceDeltaX;
    private float distanceDeltaY;

    private float origX;
    private float origY;


	// Use this for initialization
	void Start () {
        origX = this.transform.localPosition.x;
        origY = this.transform.localPosition.y;
	}
	
	// Update is called once per frame
	void Update () {
        timer += moveSpeed;
        //this.transform.localScale = new Vector2((timer % 60) / 120 + 0.5f, (timer % 60) / 120 + 0.5f);
        distanceDeltaX = Mathf.Sin(timer) * moveDistanceX;
        distanceDeltaY = Mathf.Sin(timer) * moveDistanceY;

        float posValX = origX + distanceDeltaX;
        float posValY = origY + distanceDeltaY;

        this.transform.localPosition = new Vector2(posValX, posValY);
	}
}
