﻿using UnityEngine;
using System.Collections;

public class rugae_generator_script : MonoBehaviour {

	private float timer;
	public float grumbleSpeed = 0.02f;
	public float grumbleAmplitude = 0; 
	float scaleDelta = 0;
	private float origScale;

	private GameObject player;
	private float distance;
	
	void Awake() {
		//load all rugae sprites from resources
		Sprite[] easy = Resources.LoadAll<Sprite>("stomach_rugae/easy");
		Sprite[] med = Resources.LoadAll<Sprite>("stomach_rugae/med");
		Sprite[] hard = Resources.LoadAll<Sprite>("stomach_rugae/hard");
		
		float randomNum = Random.Range (0, 100);
		
		if (randomNum >= 0 && randomNum < 60)
		{
			this.GetComponent<SpriteRenderer>().sprite = easy[ (int) Random.Range (0, easy.Length) ];
		}
		
		if (randomNum >= 60 && randomNum < 90)
		{
			this.GetComponent<SpriteRenderer>().sprite = med[ (int) Random.Range (0, med.Length) ];
		}
		
		if (randomNum >= 90 && randomNum <= 100)
		{
			this.GetComponent<SpriteRenderer>().sprite = hard[ (int) Random.Range (0, hard.Length) ];
		}

		player = GameObject.FindGameObjectWithTag("Player");
	}	

	// Use this for initialization
	void Start () {
		timer = Random.Range (0, 10);

		origScale = this.transform.localScale.x;


	
		//randomize rugae sprite


		//this.GetComponent<UnityJellySprite>().m_Sprite = sprites[ (int) Random.Range (0, sprites.Length) ];

		//this.GetComponent<SpriteRenderer>().color = new Color(0.77f, 0, 0);

		//randomize angle
		this.transform.eulerAngles = new Vector3(0, 0, Random.Range (0, 360));

		this.transform.position = new Vector2( this.transform.position.x + Random.Range (-1f, 1f), this.transform.position.y + Random.Range (-1f, 1f) );

		this.gameObject.AddComponent<PolygonCollider2D>();
	}

	void Update() {

//		distance = Vector2.Distance(this.transform.position, player.transform.position);
//
//		if (distance > 20)
//		{
//			this.GetComponent<SpriteRenderer>().enabled = false;
//		}
//		else
//		{
//			this.GetComponent<SpriteRenderer>().enabled = true;
//		}

	}
	
	// Update is called once per frame
	void FixedUpdate () {



		timer += grumbleSpeed;
		scaleDelta = Mathf.Sin(timer) * grumbleAmplitude + grumbleAmplitude;
		float scaleVal = origScale - scaleDelta;
		this.transform.localScale = new Vector2(scaleVal, scaleVal);
	
	}
}
