﻿using UnityEngine;
using System.Collections;

public class MucusEnemy : MonoBehaviour {
    public float upForce;
    public float sideForce;
    public float viewRange;
    public float maxRange = 2;
    public bool noDelay;
    public bool noSquish;
    private GameObject player;
    private bool jumping = false;
    private float origScaleY;
    private float origPosX;
    public bool mucus = true;

	GameObject mucusAudio, helperAudio;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        origScaleY = this.transform.localScale.y;
        origPosX = this.transform.position.x;

		mucusAudio = Instantiate( Resources.Load( "Audio/MucusSound" ) ) as GameObject;
		mucusAudio.transform.parent = this.transform;

        helperAudio = Instantiate(Resources.Load("Audio/JumpSound")) as GameObject;
        helperAudio.transform.parent = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if (!jumping)
        {
            if (noDelay) {
                noDelay = false;
                StartCoroutine(earlyJump());  

            } else {
                StartCoroutine(jump());  

            }
        }
        //print(this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
        if (!noSquish) {
            this.transform.localScale = new Vector3(this.transform.localScale.x, origScaleY + (this.gameObject.GetComponent<Rigidbody2D>().velocity.y / 20f), 1f);
        }
	}

    IEnumerator earlyJump() {
        jumping = true;
        yield return new WaitForSeconds(1.5f);
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(sideForce * 1, upForce));
        jumping = false;
    }

    IEnumerator jump()
    {
        jumping = true;
        yield return new WaitForSeconds(3);
        int tempDir = 1;

        if (Vector2.Distance(this.gameObject.transform.position, player.transform.position) < viewRange)
        {
            print("yo");
            if (Mathf.Abs(this.gameObject.transform.position.y - player.transform.position.y) < 2f) {
                print("yoyoyo");
                if (this.transform.position.x > player.transform.position.x) {
                    tempDir = -1;
                } else {
                    tempDir = 1;
                }
            }
            
        }
        else
        {
            if (Random.value > 0.5)
            {
                tempDir *= -1;
            }
        }

        if (Vector2.Distance(this.transform.position, new Vector2(origPosX, this.transform.position.y)) > maxRange) {
            //print("too far!");
            if (this.transform.position.x > origPosX) {
                tempDir = -1;
            } else {
                tempDir = 1;
            }
        }

        

        this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(sideForce * tempDir, upForce));
        jumping = false;
    }

	void OnCollisionEnter2D(Collision2D col)
	{
		float dist = Vector2.Distance(this.transform.position, player.transform.position);

		if (col.gameObject.tag == "Ground" && dist < 5)
		{
            if (!mucus) {
                helperAudio.GetComponent<AudioSource>().Play();

            } else {
                mucusAudio.GetComponent<AudioSource>().Play();

            }
			print ("splat");
		}

	}
}
