﻿using UnityEngine;
using System.Collections;

public class foodDropper : MonoBehaviour {

    public int dropRate;
    public GameObject dropItem;

    


	// Use this for initialization
	void Start () {
        
        InvokeRepeating("drop", 0f, dropRate);
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void drop()
    {

        //Debug.Log("drop it like its hot");
        Instantiate(dropItem, this.transform.position, Quaternion.identity);
        //dropItem.transform.parent = this.transform;

    }
}
