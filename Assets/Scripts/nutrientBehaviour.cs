﻿using UnityEngine;
using System.Collections;

public class nutrientBehaviour : MonoBehaviour {
    public float timer = 0;
    float scaleDelta = 0;
    private GameObject nutAud;
    private GameObject player;

	// Use this for initialization
	void Start () {
        nutAud = Instantiate(Resources.Load("Audio/NutrientSound")) as GameObject;
        nutAud.transform.parent = this.transform;
	    
	}
	
	// Update is called once per frame
	void Update () {
        timer+=0.1f;
        //this.transform.localScale = new Vector2((timer % 60) / 120 + 0.5f, (timer % 60) / 120 + 0.5f);
        scaleDelta = Mathf.Sin(timer) * 0.1f;
        float scaleVal = 1 + scaleDelta;
        this.transform.localScale = new Vector2(scaleVal, scaleVal);
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        //		if (col.gameObject.tag == "Ground")
        //		{
        //			this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        //		}

        if (col.gameObject.tag == "Player")
        {
            nutAud.GetComponent<AudioSource>().Play();
            print("play audio");

            hide();
        }
    }

    void hide() {

        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f); 

        Invoke("kill", 2f);

    }

    void kill() {
        print("kill");

        Destroy(this.gameObject);
    }

}
