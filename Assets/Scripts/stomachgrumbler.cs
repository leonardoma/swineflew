﻿using UnityEngine;
using System.Collections;

public class stomachgrumbler : MonoBehaviour {

    public float timer = 0;
    float scaleDelta = 0;
    public float grumbleSpeed = 0.02f;
    public float grumbleAmplitude = 0;    
    public bool grumbling = false;
    private float origScale;

	// Use this for initialization
	void Start () {
        origScale = this.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
        timer += grumbleSpeed;
        //this.transform.localScale = new Vector2((timer % 60) / 120 + 0.5f, (timer % 60) / 120 + 0.5f);
        scaleDelta = Mathf.Sin(timer) * grumbleAmplitude;
        float scaleVal = origScale + scaleDelta;
        this.transform.localScale = new Vector2(scaleVal, scaleVal);
        //this.gameObject.transform.rotation = Quaternion.Euler(0,0,0);


        //this.gameObject.GetComponent<SpriteRenderer>().o
	}

    void FixedUpdate () { 
        //this.gameObject.transform.rotation.x = 0f;
        //this.gameObject.transform.rotation.y = 0f;
        //this.gameObject.transform.localRotation.x = 0f;

    }

    public void callGrumble(float duration) {
        if (!grumbling)
        {
            grumbling = true; 
            StartCoroutine(Grumble(duration));
        }
    }

    IEnumerator Grumble(float duration)
    {
        grumbleSpeed = grumbleSpeed * 25f;
        yield return new WaitForSeconds(duration);
        grumbleSpeed = grumbleSpeed / 25f;
        grumbling = false;
    }
}
