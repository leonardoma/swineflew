﻿using UnityEngine;
using System.Collections;

public class Tongue : MonoBehaviour {

    public Transform uvula;

    private bool mouthOpen = false;
    private float origX, origY;
    private float curAgitation;
    private float agitationVal = 3.5f;

    // Use this for initialization
    void Start() {
        origX = this.transform.position.x;

        origY = this.transform.position.y;
        //uvula.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 1);
    }

    // Update is called once per frame
    void Update() {
        float agitation = Mathf.Abs(uvula.gameObject.GetComponent<Rigidbody2D>().velocity.x) + Mathf.Abs(uvula.gameObject.GetComponent<Rigidbody2D>().velocity.y);
        if (agitation > curAgitation) {
            curAgitation = agitation;
        } else {
            curAgitation -= 0.01f;
        }

        //uvula.GetComponent<SpriteRenderer>().color = new Color(curAgitation / agitationVal, curAgitation / agitationVal, curAgitation / agitationVal, 1);

        transform.rotation = Quaternion.Euler(0, 0, curAgitation * (-2));
        



        //this.transform.position = new Vector3(this.transform.position.x, origY-Mathf.Abs(uvula.gameObject.GetComponent<Rigidbody2D>().velocity.x), this.transform.position.z);
    }
}