﻿using UnityEngine;
using System.Collections;

public class particle_script : MonoBehaviour {

	float counter = 0;
	public float timer;

	//private bool circleSize = true;
	public float decreaseSizePerFrame;

	// Use this for initialization
	void Start () {


		this.transform.position = this.transform.parent.position;
	
	}
	
	// Update is called once per frame
	void Update () {

		counter ++;

		if (counter > timer)
		{
			Destroy (this.gameObject);
		}

//		if (GetComponent<Rigidbody2D>().velocity.magnitude < 0.5f && !circleSize)
//		{
//			this.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
//			circleSize = true;
//		}
//
		//if (circleSize && this.transform.localScale.x >= 0.15f)
		//if (GetComponent<Rigidbody2D>().velocity.magnitude < 0.5f)
		{
			this.transform.localScale = new Vector3(this.transform.localScale.x - decreaseSizePerFrame, this.transform.localScale.y - decreaseSizePerFrame, this.transform.localScale.z - decreaseSizePerFrame);
		}


	
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Ground")
		{
            
		}

	}
}
