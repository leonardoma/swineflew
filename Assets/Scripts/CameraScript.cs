﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public GameObject player;
	private Vector3 playerPos;

	public float camRangeX, camRangeY, camOffset, camHorizSpeed, camVertSpeed;
    public float minX, maxX, minY, maxY;
    public float prefOrthoSize = 4f;
	private float camX, camY;

	private float startX = 0;
	private float startY = 0;
	private float startSize = 0;
	private bool startZoom = false;

	bool movingRight, movingUp;
	
	private spriteMovement2 pm_cache;

	private float avgX, avgY, avgZ;

	private Vector2 camPos;

	private bool allPlayersVisible = true;
	private bool decreaseCamSize = false;

	private float deltaAmountX = 0.2f;
	private float deltaAmountY = 0.2f;

	private Camera cam;

	private float camSize = 4;
	private float camSizeDecreaseSpeed = 0.03f;
	private  float counter = 0;

	private bool zoomBool = false;
	private float zoomStart, zoomMiddle, zoomEnd;
	private float zoomCounter = 100;

	private Vector2 camDestination;
	private float camDestSize;

	KeyCode jump_joystick, split_joystick, join_joystick, stick_joystick;

	void Awake() {

		//lock frame rate
		Application.targetFrameRate = 60;

	}

	// Use this for initialization
	void Start () {

//		camRangeX = 0.5f;
//		camRangeY = 0.5f;


		player = GameObject.FindGameObjectWithTag("Player");


		pm_cache = player.GetComponent<spriteMovement2>();

		camX = player.transform.position.x;
		camY = player.transform.position.y;

		camPos = new Vector2(camX, camY);

		cam = this.GetComponent<Camera>();

		switch(Application.loadedLevelName) {
		case "stomach_sean":	startX = 2f;
								startY = -2f;
								startSize = 20f;
								break;
		case "test":			startX = 0f;
								startY = 0f;
								startSize = 21.5f;
								break;
		case "duodenum":		startX = -3.25f;
								startY = -10.95f;
								startSize = 11f;
								break;
		case "nasal":			startX = 6.64f;
								startY = -20f;
								startSize = 20f;
								break;
		}

		if (Application.loadedLevelName == PlayerPrefs.GetString("prevLevelName"))
		{
			startZoom = false;
		}
		else
		{
			startZoom = true;
		}

		print (PlayerPrefs.GetString("prevLevelName") + " : " + Application.loadedLevelName);

		PlayerPrefs.SetString("prevLevelName", Application.loadedLevelName);


		GameObject bgAudioClone = Instantiate( Resources.Load("Audio/BG Audio") ) as GameObject;
		bgAudioClone.transform.parent  = this.transform;

		GameObject pauseScreen = Instantiate( Resources.Load("Prefabs/Pause_Canvas") ) as GameObject;
	
		if (Input.GetJoystickNames ().Length > 0)
		{
			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				split_joystick = KeyCode.JoystickButton12;
				join_joystick = KeyCode.JoystickButton13;
				stick_joystick = KeyCode.JoystickButton9;
				
				print ("ps3 controller connected");
			}
			
			//ps4

			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				split_joystick = KeyCode.JoystickButton3;  
				join_joystick = KeyCode.JoystickButton2;
				stick_joystick = KeyCode.JoystickButton7;
				
				print ("ps4 controller connected");
			}

            print(Input.GetJoystickNames()[0]);
		}



	}

	IEnumerator JumpCameraTrackingSpeed()
	{
		deltaAmountY = 0.05f;
		yield return new WaitForSeconds(1.0f);
		deltaAmountY = 0.2f;
		yield return null;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Space) || (Input.GetKeyDown(jump_joystick) ))
		{
			StopCoroutine("JumpCameraTrackingSpeed");
			StartCoroutine("JumpCameraTrackingSpeed");
		}

//		if (Input.GetKeyDown (KeyCode.A))
//		{
//			//StartFeedbackZoom(Vector2 camDestination, float camSizeArg, float start, float middle, float end)
//			StartFeedbackZoom(new Vector2(-8.36f, 6.35f), 5, 2, 3, 5);
//		}

		avgX = 0;
		avgY = 0;
		avgZ = 0;

		//avg values
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			avgX += go.transform.position.x / GameObject.FindGameObjectsWithTag("Player").Length;
			avgY += go.transform.position.y / GameObject.FindGameObjectsWithTag("Player").Length;
			avgZ += go.transform.position.z / GameObject.FindGameObjectsWithTag("Player").Length;
		}

		playerPos = new Vector3(avgX, avgY, avgZ);



		camX = Mathf.Clamp(playerPos.x, minX, maxX);
		camY = Mathf.Clamp(playerPos.y, minY, maxY);

		//this.transform.position = new Vector3(camX, camY, this.transform.position.z);


		float deltaX = Mathf.Clamp(camX - camPos.x, -deltaAmountX, deltaAmountX);
		float deltaY = Mathf.Clamp(camY - camPos.y, -deltaAmountY, deltaAmountY);

		camPos = new Vector2( camPos.x + deltaX, camPos.y + deltaY);

		if (startZoom)
		{
			StartZoomIn();
		}

		if (zoomBool)
		{
			FeedbackZoom();
		}
		
		this.transform.position = new Vector3(camPos.x, camPos.y, this.transform.position.z);

		//check if every player gameobject visible
		Vector2 bottomLeft = cam.ViewportToWorldPoint(new Vector3(0.2f, 0.2f, cam.nearClipPlane));
		Vector2 topRight = cam.ViewportToWorldPoint(new Vector3(0.8f, 0.8f, cam.nearClipPlane));

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			bool horiz = go.transform.position.x < bottomLeft.x || go.transform.position.x > topRight.x;
			bool vert = go.transform.position.y < bottomLeft.y || go.transform.position.y > topRight.y;

			if ( horiz || vert )
			{
				allPlayersVisible = false;
				break;
			}
			else
			{
				allPlayersVisible = true;
			}
		}

		if (!allPlayersVisible)
		{
			this.GetComponent<Camera>().orthographicSize += 0.03f;
		}

		//check if it's okay to decrease camera size
		Vector2 newBottomLeft = cam.ViewportToWorldPoint(new Vector3(0.3f, 0.3f, GetComponent<Camera>().nearClipPlane));
		Vector2 newTopRight = cam.ViewportToWorldPoint(new Vector3(0.7f, 0.7f, GetComponent<Camera>().nearClipPlane));

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			bool horiz = go.transform.position.x > newBottomLeft.x && go.transform.position.x < newTopRight.x;
			bool vert = go.transform.position.y > newBottomLeft.y && go.transform.position.y < newTopRight.y;
			
			if ( horiz && vert )
			{
				decreaseCamSize = true;
				break;
			}
			else
			{
				decreaseCamSize = false;
			}
		}

		if (decreaseCamSize && this.GetComponent<Camera>().orthographicSize > prefOrthoSize)
		{
			this.GetComponent<Camera>().orthographicSize -= camSizeDecreaseSpeed;
		}
			                          
	
	}

	void StartZoomIn() {

		if (counter < 1)
		{
			camX = startX;
			camY = startY;
			camPos = new Vector2(camX, camY);
			cam.orthographicSize = startSize;
		}


		if (counter >= 1.0f && counter < 2.0f)
		{
			camX = Mathf.Lerp(startX, playerPos.x, counter-1);
			camY = Mathf.Lerp(startY, playerPos.y, counter-1);
			camPos = new Vector2(camX, camY);
			cam.orthographicSize = Mathf.Lerp(startSize, prefOrthoSize, counter-1);
		}

		counter += 0.01f;
	}

	public void StartFeedbackZoom(Vector2 camDestination, float camSizeArg, float start, float middle, float end) {

		if (!zoomBool)
		{
			zoomBool = true;
			zoomCounter = 0;
			this.camDestination = camDestination;
			this.camDestSize = camSizeArg;
			zoomStart = start;
			zoomMiddle = middle;
			zoomEnd = end;
		}


	}

	void FeedbackZoom()
	{
		if (zoomCounter < zoomStart)
		{
			foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
			{
				pl.GetComponent<spriteMovement2>().enabled = false;
			}

			camX = Mathf.Lerp(playerPos.x, camDestination.x, (zoomCounter) / zoomStart);
			camY = Mathf.Lerp(playerPos.y, camDestination.y, (zoomCounter) / zoomStart);
			camPos = new Vector2(camX, camY);
			cam.orthographicSize = Mathf.Lerp(prefOrthoSize, camDestSize, (zoomCounter) / zoomStart);

			foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
			{
				if (go.transform.name.Contains("WhiteBlood") || go.transform.name.Contains("MucusEnemy"))
				{
					go.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
				}
			}
		}
		
		if (zoomCounter >= zoomStart && zoomCounter < zoomMiddle)
		{
			camX = camDestination.x;
			camY = camDestination.y;
			camPos = new Vector2(camX, camY);
			cam.orthographicSize = camDestSize;
		}
		
		if (zoomCounter >= zoomMiddle && zoomCounter < zoomEnd)
		{
			camX = Mathf.Lerp( camDestination.x, playerPos.x, (zoomCounter - zoomMiddle) / (zoomEnd - zoomMiddle) );
			camY = Mathf.Lerp( camDestination.y, playerPos.y, (zoomCounter - zoomMiddle) / (zoomEnd - zoomMiddle) );
			camPos = new Vector2(camX, camY);
            cam.orthographicSize = Mathf.Lerp(camDestSize, prefOrthoSize, (zoomCounter - zoomMiddle) / (zoomEnd - zoomMiddle));
		}
		
		if (zoomCounter >= zoomEnd)
		{
			foreach(GameObject pl in GameObject.FindGameObjectsWithTag("Player"))
			{
				pl.GetComponent<spriteMovement2>().enabled = true;
			}

			foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
			{
				if (go.transform.name.Contains("WhiteBlood") )
				{
					go.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
				}

				if (go.transform.name.Contains("MucusEnemy"))
				{
					go.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
				}
			}

			zoomBool = false;
			zoomCounter = 0;
		}
		
		zoomCounter+= Time.deltaTime;
	}

	bool checkMovingRight()
	{
		if (playerPos.x >= camX + camOffset)
		{
			return true;
		}

		if (playerPos.x <= camX - camOffset )
		{
			return false;
		}

		return true;
	}
}
