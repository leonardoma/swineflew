﻿using UnityEngine;
using System.Collections;

public class gumController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionStay2D(Collision2D col)
    {
        //Debug.Log("pusher");

        if (col.gameObject.tag == "Player" && col.gameObject.transform.localScale.x == 1)
        {
            this.GetComponent<Rigidbody2D>().mass = 50;
            Debug.Log("large pusher");
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            this.GetComponent<Rigidbody2D>().mass = 300;
        }
    }
}
