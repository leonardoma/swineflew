﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class startMenu_script : MonoBehaviour {

	public GameObject panel;

	bool analog_cooldown = true;
	public int buttonCase = 0;

	public GameObject StartButton, CreditsButton, creditsObj;

	Color preColor;

	Color active_color, inactive_color;

	KeyCode pause_joystick, pad_joystick, up_joystick, down_joystick, jump_joystick;

	bool pressed = false;
	float alpha = 0;

	// Use this for initialization
	void Start () {

		PlayerPrefs.DeleteAll ();

		preColor = panel.GetComponent<Image>().color;

		active_color = new Color(1, 1, 1, 1);
		inactive_color = new Color(1, 1, 1, 0.5f);

		StartButton.GetComponent<Image>().color = active_color;
		CreditsButton.GetComponent<Image>().color = inactive_color;

		if (Input.GetJoystickNames ().Length > 0)
		{

			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				pause_joystick = KeyCode.JoystickButton3; 
				print("pause listening");
				
				up_joystick = KeyCode.JoystickButton4;
				down_joystick = KeyCode.JoystickButton6;
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				pause_joystick = KeyCode.JoystickButton9; 
				pad_joystick = KeyCode.JoystickButton13;
				
				//up down buttons are also axes
			}

		}
	
	}
	
	// Update is called once per frame
	void Update () {

		if (pressed){
			panel.GetComponent<Image>().color = new Color(preColor.r, preColor.g, preColor.b, alpha);

			alpha += 0.01f;
		}

		if (alpha > 1.1f)
		{
			Application.LoadLevel("intro2");
		}

		if (Input.GetKeyDown(up_joystick) || Input.GetKeyDown(KeyCode.UpArrow) || (Input.GetAxis("Vertical") > 0 && analog_cooldown) || (Input.GetAxis("PS4_DpadY") < 0 && analog_cooldown) )
		{
			buttonCase --;
			analog_cooldown = false;
		}
		
		if (Input.GetKeyDown(down_joystick) || Input.GetKeyDown(KeyCode.DownArrow) || (Input.GetAxis("Vertical") < 0 && analog_cooldown) || (Input.GetAxis("PS4_DpadY") > 0 && analog_cooldown) )
		{
			buttonCase ++;
			analog_cooldown = false;
		}
		
		if (Input.GetAxis ("Vertical") == 0 && Input.GetAxis("PS4_DpadY") == 0)
		{
			analog_cooldown = true;
		}
		
		if (buttonCase < 0)
		{
			buttonCase = 1;
		}
		
		if (buttonCase > 1)
		{
			buttonCase = 0;
		}

		switch(buttonCase) {
		case 0: StartButton.GetComponent<Image>().color = active_color;
			CreditsButton.GetComponent<Image>().color = inactive_color;
			break;
		case 1: StartButton.GetComponent<Image>().color = inactive_color;
			CreditsButton.GetComponent<Image>().color = active_color;
			break;
		}

		if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown (jump_joystick) || Input.GetKeyDown (pause_joystick))
		{
			switch(buttonCase) {
				//unpause
			case 0: StartPressed();
				print ("starting up");
				break;
			case 1: CreditsPressed();
				break;
			}
		}
	
	}

	public void StartPressed() {

		panel.SetActive(true);
		pressed = true;
	}

	public void CreditsPressed() {
		print ("crediting up");
		creditsObj.SetActive(true);
		this.gameObject.SetActive(false);
	}
}
