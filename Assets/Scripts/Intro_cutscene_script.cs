﻿using UnityEngine;
using System.Collections;

public class Intro_cutscene_script : MonoBehaviour {

	public bool IntestineLevel = false;

	float timer = 0;

	int counter;

	KeyCode pause_joystick, pad_joystick, up_joystick, down_joystick, jump_joystick; 

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetString("prevLevelName", "introCutscene");

		if (Input.GetJoystickNames ().Length > 0)
		{
			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				pause_joystick = KeyCode.JoystickButton3; 
				print("pause listening");
				
				up_joystick = KeyCode.JoystickButton4;
				down_joystick = KeyCode.JoystickButton6;
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				pause_joystick = KeyCode.JoystickButton9; 
				pad_joystick = KeyCode.JoystickButton13;
				
				//up down buttons are also axes
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (IntestineLevel)
		{
			timer += 0.1f;
		}

		if (Input.GetKeyDown(jump_joystick))
		{
			counter ++;
		}


		if (timer > 1 || counter > 2)
		{
			Application.LoadLevel("test");
		}
	
	}
}
