﻿using UnityEngine;
using System.Collections;

public class eye : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GameObject tempPlayer = GameObject.FindGameObjectWithTag("Player");
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(tempPlayer.transform.position.y - transform.position.y, tempPlayer.transform.position.x - transform.position.x) * Mathf.Rad2Deg - 90);
	}
}
