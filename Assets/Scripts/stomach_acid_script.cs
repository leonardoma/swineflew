﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class stomach_acid_script : MonoBehaviour {

	private float speed;
	public GameObject acidCollider;

	private Vector2 pos;

	private float distPlayerAcid;

	private bool rising = false;

	private float timer = 0;

	// Use this for initialization
	void Start () {

		pos = this.GetComponent<RectTransform>().anchoredPosition;


	
	}
	
	// Update is called once per frame
	void Update () {




		timer += 0.035f;

		distPlayerAcid = GameObject.FindGameObjectWithTag("Player").transform.position.y - acidCollider.transform.position.y;

		if ( GameObject.FindGameObjectWithTag("Player").transform.position.x > -17 )
		{
			rising = true;
		}

		if ( GameObject.FindGameObjectWithTag("Player").transform.position.y > 20 )
		{
			rising = false;
		}



		if (distPlayerAcid > 4.1f && rising)
		{
			speed =  Random.Range(0.15f, 0.2f);
		}
		if (distPlayerAcid <= 4.1f && rising)
		{
			speed = Random.Range(0.005f, 0.010f);
		}
		if (distPlayerAcid > 7f && rising)
		{
			speed = Random.Range (0.05f, 0.07f);
		}

		if (!rising)
		{
			speed = 0;
		}

		float sinX = Mathf.Sin (timer) * 2;
		pos.y += speed;

		pos = new Vector2(sinX, pos.y + Mathf.Sin (timer) * 0.04f);

		this.GetComponent<RectTransform>().anchoredPosition = pos;
	
	}
}
