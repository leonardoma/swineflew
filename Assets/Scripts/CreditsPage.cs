﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditsPage : MonoBehaviour {

	KeyCode pause_joystick, pad_joystick, up_joystick, down_joystick, jump_joystick;

	public GameObject MainScreen, backButton;

	bool analog_cooldown = true;

	// Use this for initialization
	void Start () {

		backButton.GetComponentInChildren<Text>().color =  new Color(1, 1, 1, 0.6f);

		if (Input.GetJoystickNames ().Length > 0)
		{

			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				pause_joystick = KeyCode.JoystickButton3; 
				print("pause listening");
				
				up_joystick = KeyCode.JoystickButton4;
				down_joystick = KeyCode.JoystickButton6;
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				pause_joystick = KeyCode.JoystickButton9; 
				pad_joystick = KeyCode.JoystickButton13;
				
				//up down buttons are also axes
			}

		}
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(pause_joystick) || Input.GetKeyDown(pad_joystick) || Input.GetKeyDown(jump_joystick))
		{
			BackToMainScreen();
		}

		if (Input.GetKeyDown(up_joystick) || Input.GetKeyDown(KeyCode.UpArrow) || (Input.GetAxis("Vertical") > 0 && analog_cooldown) || (Input.GetAxis("PS4_DpadY") < 0 && analog_cooldown) )
		{
			backButton.GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1.0f);
			analog_cooldown = false;
		}
		
		if (Input.GetKeyDown(down_joystick) || Input.GetKeyDown(KeyCode.DownArrow) || (Input.GetAxis("Vertical") < 0 && analog_cooldown) || (Input.GetAxis("PS4_DpadY") > 0 && analog_cooldown) )
		{
			backButton.GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1.0f);
			analog_cooldown = false;
		}
		
		if (Input.GetAxis ("Vertical") == 0 && Input.GetAxis("PS4_DpadY") == 0)
		{
			analog_cooldown = true;
		}
	
	}

	public void BackToMainScreen() 
	{
		this.gameObject.SetActive(false);
		MainScreen.gameObject.SetActive(true);
	}
}
