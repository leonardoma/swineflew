﻿using UnityEngine;
using System.Collections;

public class SplitTut : MonoBehaviour {
    public GameObject helper1, helper2;
    public GameObject text1, text2;
	// Use this for initialization
	void Start () {
        StartCoroutine(Join());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator Join() {
        text1.SetActive(false);
        text2.SetActive(true);
        helper1.SetActive(false);
        helper2.transform.localPosition = new Vector3(0f, 0f);
        helper2.transform.localScale = new Vector3(1f, 1f, 1f);
        yield return new WaitForSeconds(2f);
        StartCoroutine(Split());
    }

    IEnumerator Split() {
        text1.SetActive(true);
        text2.SetActive(false);
        helper1.SetActive(true);
        helper2.transform.localPosition = new Vector3(-0.3f, 0f);
        helper2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        yield return new WaitForSeconds(2f);
        StartCoroutine(Join());
    }  
}
