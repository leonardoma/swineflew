﻿using UnityEngine;
using System.Collections;

public class TextPopup : MonoBehaviour {
    public bool showText = false;

    public float textTimer = 1.1f; 
    private float origScale, origTimer; 

    private bool showed = false;


	// Use this for initialization
	void Start () {
        origTimer = textTimer;
        origScale = this.transform.localScale.x;
        this.transform.localScale = new Vector3(0f, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
        if (!showed) {
            if (showText) {
                textTimer *= origTimer;

                if (textTimer < 26f) {
                    this.transform.localScale = new Vector3(textTimer / 80f, textTimer / 80f, 0f);
                    this.transform.rotation = Quaternion.Euler(0f, 0f, textTimer * 15f);
                } else if (textTimer < 160f) {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + (textTimer / 1280f), 0f);

                } else if (textTimer <= 1600f) {
                    this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f - textTimer / 1600f);
                } else {
                    showed = true;
                    showText = false;
                    textTimer = 1.1f;
                    this.transform.localScale = new Vector3(0f, 0f, 0f);
                    this.transform.localPosition = new Vector3(0f, 0f, 0f);
                    this.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                    this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

                }
            }
        }
	}
}
