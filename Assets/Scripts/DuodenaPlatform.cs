﻿using UnityEngine;
using System.Collections;

public class DuodenaPlatform : MonoBehaviour
{
	
	private float timer = 0;
	public float moveSpeed;
	public float moveMultiplier;

	void Start ()
	{

	}

	void Update ()
	{

		transform.localScale += new Vector3 (0, Mathf.Sin (timer)*moveMultiplier, 0);

		timer += moveSpeed;

	}
}
