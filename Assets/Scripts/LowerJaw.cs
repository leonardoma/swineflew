﻿using UnityEngine;
using System.Collections;

public class LowerJaw : MonoBehaviour {

    public Transform uvula; 

    private bool mouthOpen = false;
    private float origX, origY;
    private float curAgitation;
    private float agitationVal = 3.5f;

	// Use this for initialization
	void Start () {
        origX = this.transform.position.x;

        origY = this.transform.position.y;
        uvula.GetComponent<SpriteRenderer>().color = new Color(0.55f,0.1f,0.1f,1);
	}
	
	// Update is called once per frame
	void Update () {
        float agitation = Mathf.Abs(uvula.gameObject.GetComponent<Rigidbody2D>().velocity.x) + Mathf.Abs(uvula.gameObject.GetComponent<Rigidbody2D>().velocity.y);
        if (agitation > curAgitation) {
            curAgitation = agitation;
        } else {
            curAgitation -= 0.01f;
        }

        uvula.GetComponent<SpriteRenderer>().color = new Color(0.55f + curAgitation / agitationVal, 0.1f + curAgitation / agitationVal, 0.1f + curAgitation / agitationVal, 1);

        this.transform.position = new Vector3(origX, origY - curAgitation/3, 1f);

        

        //this.transform.position = new Vector3(this.transform.position.x, origY-Mathf.Abs(uvula.gameObject.GetComponent<Rigidbody2D>().velocity.x), this.transform.position.z);
	}
}
