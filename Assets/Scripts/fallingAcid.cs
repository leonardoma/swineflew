﻿using UnityEngine;
using System.Collections;

public class fallingAcid : MonoBehaviour {

    public bool stopDrip;
    public bool startDrip;
    public float dripSpeed = 0.05f;
    public bool shrinkBoth;
    private float origScale;
    public float minScale;

	// Use this for initialization
	void Start () {
        origScale = this.gameObject.transform.localScale.y;
	}
	
	// Update is called once per frame
	void Update () {
        /*if (startDrip) {
            //print("start");
        }

        if (stopDrip) {
            //print("stop");
        }

        if (stopDrip)
        {
            //startDrip = false;

            if (this.gameObject.transform.localScale.y >= minScale)
            {
                this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y - dripSpeed/10, 1f);
                if (shrinkBoth) {
                    this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x - (dripSpeed*1.5f), this.gameObject.transform.localScale.y, 1f);

                }
            }
            else
            {
                stopDrip = false;
            }

        }

        if (startDrip)
        {
            //stopDrip = false; 
        */
            if (this.gameObject.transform.localScale.y <= origScale) 
            {
                if (!shrinkBoth) {
                    this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y + (dripSpeed / 10), 1f);

                }
                if (shrinkBoth) {
                    //this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x + ((dripSpeed*1.5f) / 10), this.gameObject.transform.localScale.y, 1f);

                }
                startDrip = false;
            }
                /*
            else
            {
               startDrip = false;
            }

            
        }*/
	}

    /*
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "movingplatform" && this.transform.localScale.y >= origScale)
        {
            //startDrip = false;

            stopDrip = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        print("yep");
        if (col.gameObject.name == "movingplatform" && this.transform.localScale.y <= origScale)
        {
            //stopDrip = false;

            startDrip = true;

        }
    }
     */

    void OnTriggerStay2D(Collider2D col) {
        if (col.gameObject.name == "movingplatform") {
            if (!startDrip) {
                startDrip = true;
            }

            if (this.gameObject.transform.localScale.y >= minScale) {
                this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y - dripSpeed, 1f);
                if (shrinkBoth) {
                    this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x - (dripSpeed * 1.5f), this.gameObject.transform.localScale.y, 1f);

                }
            }
        }
        
    }

}
