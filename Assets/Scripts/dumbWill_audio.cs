﻿using UnityEngine;
using System.Collections;

public class dumbWill_audio : MonoBehaviour {

	public AudioClip startYay;
	public AudioClip[] jumpNoises;

	// Use this for initialization
	void Start () {

		//start noise
		this.GetComponent<AudioSource>().clip = startYay;

		this.GetComponent<AudioSource>().pitch = Mathf.Pow( 1.05946f, Random.Range (1, 4) );

		this.GetComponent<AudioSource>().Play ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//jump noises
	public void PlayJumpSound()
	{
		this.GetComponent<AudioSource>().pitch = Mathf.Pow( 1.05946f, Random.Range (-4, 4) );

		this.GetComponent<AudioSource>().clip = jumpNoises[ Mathf.FloorToInt(  Random.Range (0, jumpNoises.Length) ) ];
		
		this.GetComponent<AudioSource>().Play ();
	}
}
