﻿using UnityEngine;
using System.Collections;

public class stomach_food_script : MonoBehaviour {

	private float timerDestroy = 500;
	private float timer = 0;

	public Sprite[] foodSprites;

	private SpriteRenderer sr;
	private float alpha = 1.0f;

	GameObject audioPre;

	// Use this for initialization
	void Start () {

		int randomNum = (int) Random.Range (0, foodSprites.Length);
		this.GetComponent<SpriteRenderer>().sprite = foodSprites[ randomNum ];

		this.gameObject.AddComponent<PolygonCollider2D>();

		sr = this.GetComponent<SpriteRenderer>();
//	
//		audioPre = Instantiate(Resources.Load("Audio/Food_audio")) as GameObject;
//		audioPre.GetComponent<AudioSource>().Play ();
//		audioPre.transform.parent = this.transform;

	}

	void FadeFood() {

		alpha -= 0.1f;
		sr.color = new Color(1,1,1,alpha);

		if (alpha < -0.1f)
		{
			GameObject.Destroy(this.gameObject);
		}

	}
	
	// Update is called once per frame
	void Update () {

		timer++;

		if (timer > timerDestroy)
		{
			FadeFood ();
		}


	
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.name == "acid_Collider")
		{
			this.GetComponent<Collider2D>().enabled = false;
			this.GetComponent<Rigidbody2D>().drag = 30f;
			this.GetComponent<Rigidbody2D>().angularDrag = 30f;
		}
	}
}
