﻿using UnityEngine;
using System.Collections;

public class hideOnCue : MonoBehaviour {

    public GameObject cueGiver;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (cueGiver.GetComponent<setHideCue>().hide)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);

        }
	}
}
