﻿using UnityEngine;
using System.Collections;

public class mapSwitcher : MonoBehaviour {

    public bool on;
    public GameObject map1;
    public GameObject map2;
    public GameObject visComponent;
    public string newVisComponentLayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            map1.GetComponent<PolygonCollider2D>().isTrigger = on;
            
            map2.GetComponent<PolygonCollider2D>().isTrigger = !on;

            if (on)
            {
                map1.tag = "Untagged";
                map2.tag = "Ground";


            }
            else
            {
                map1.tag = "Ground";
                map2.tag = "Untagged";

            }

            visComponent.GetComponent<SpriteRenderer>().sortingLayerName = newVisComponentLayer;



        }
    }
}
