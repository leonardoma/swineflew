﻿using UnityEngine;
using System.Collections;

public class altTutText : MonoBehaviour {
    public Sprite altText;
	// Use this for initialization
	void Start () {
	    if (Input.GetJoystickNames ().Length > 0) {
            //joystickConnected
            if (altText != null) {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = altText;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
