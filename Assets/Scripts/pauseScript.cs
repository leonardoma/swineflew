﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pauseScript : MonoBehaviour {

	public bool paused;

	public GameObject pauseBG;

	KeyCode pause_joystick, pad_joystick, up_joystick, down_joystick, jump_joystick; 

	int buttonCase = 0;

	public GameObject resume, mainscreen, restart;

	Color active_color, inactive_color;

	bool analog_cooldown = false;

	// Use this for initialization
	void Start () {

		paused = false;

		buttonCase = 0;

		pauseBG.SetActive(false);

		if (Input.GetJoystickNames ().Length > 0)
		{

			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				pause_joystick = KeyCode.JoystickButton3; 
				print("pause listening");

				up_joystick = KeyCode.JoystickButton4;
				down_joystick = KeyCode.JoystickButton6;
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				pause_joystick = KeyCode.JoystickButton9; 
				pad_joystick = KeyCode.JoystickButton13;

				//up down buttons are also axes
			}

		}

		active_color = new Color(1, 1, 1, 1);
		inactive_color = new Color(1, 1, 1, 0.5f);
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(pause_joystick) || Input.GetKeyDown(pad_joystick))
		{
			if (paused)
			{
				UnPause();
			}
			else
			{
				Time.timeScale = 0f;
				pauseBG.SetActive(true);
				paused = true;
			}
		}

		switch(buttonCase) {
		case 0: resume.GetComponent<Image>().color = active_color;
				mainscreen.GetComponent<Image>().color = inactive_color;
				restart.GetComponent<Image>().color = inactive_color;
				break;
		case 1: resume.GetComponent<Image>().color = inactive_color;
				mainscreen.GetComponent<Image>().color = active_color;
				restart.GetComponent<Image>().color = inactive_color;
				break;
		case 2: resume.GetComponent<Image>().color = inactive_color;
				mainscreen.GetComponent<Image>().color = inactive_color;
				restart.GetComponent<Image>().color = active_color;
				break;
		}

		if (paused)
		{
			if (Input.GetKeyDown(up_joystick) || Input.GetKeyDown(KeyCode.UpArrow) || (Input.GetAxis("Vertical") > 0 && analog_cooldown) || (Input.GetAxis("PS4_DpadY") < 0 && analog_cooldown) )
			{
				buttonCase --;
				analog_cooldown = false;
				
				print ("someone pressing up");
			}
			
			if (Input.GetKeyDown(down_joystick) || Input.GetKeyDown(KeyCode.DownArrow) || (Input.GetAxis("Vertical") < 0 && analog_cooldown) || (Input.GetAxis("PS4_DpadY") > 0 && analog_cooldown) )
			{
				buttonCase ++;
				analog_cooldown = false;
			}
			
			if (Input.GetAxis ("Vertical") == 0 && Input.GetAxis("PS4_DpadY") == 0)
			{
				analog_cooldown = true;
			}
			
			if (buttonCase < 0)
			{
				buttonCase = 2;
			}
			
			if (buttonCase > 2)
			{
				buttonCase = 0;
			}

			if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown (jump_joystick))
			{
				switch(buttonCase) {
				case 0: UnPause();
					break;
				case 1:	RestartLevel();
					break;
				case 2: MainScreen();
					break;
				}
			}
		}

	}

	public void UnPause() {
		Time.timeScale = 1f;
		pauseBG.SetActive(false);
		buttonCase = 0;
		paused = false;
	}

	public void RestartLevel() {
		Time.timeScale = 1f; 
		//PlayerPrefs.DeleteAll ();
		Application.LoadLevel(Application.loadedLevel);
	}

	public void MainScreen() {
		Time.timeScale = 1f; 
		PlayerPrefs.DeleteAll ();
		Application.LoadLevel(0);
	}
}
