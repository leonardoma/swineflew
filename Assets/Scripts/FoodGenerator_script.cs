﻿using UnityEngine;
using System.Collections;

public class FoodGenerator_script : MonoBehaviour {

	public GameObject[] foodObjects;

	public float rateOfFire;

	private float timer;

	bool droppingFoodBool = false;

	// Use this for initialization
	void Start () {

		timer = Random.Range (0, rateOfFire);
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		timer ++;

		float vertDist = this.transform.position.y - GameObject.FindGameObjectWithTag("Player").transform.position.y;

		if (timer > rateOfFire && droppingFoodBool)
		{
			//generate food
			//int randomNum = (int)Random.Range (0, foodObjects.Length );
			GameObject tempFoodObj = Instantiate ( foodObjects[ 0 ] ) as GameObject;
			tempFoodObj.transform.position = this.transform.position;
			tempFoodObj.transform.localScale = new Vector2(0.5f, 0.5f);
			tempFoodObj.GetComponent<Rigidbody2D>().velocity = new Vector2( Random.Range (-10, 10), tempFoodObj.GetComponent<Rigidbody2D>().velocity.y);
			tempFoodObj.transform.parent = this.transform;
			timer = 0;
		}

		if (vertDist > 5 && vertDist < 15)
		{
			droppingFoodBool = true;
		}
		else
		{
			droppingFoodBool = false;
		}
	


		//float dist = Vector2.Distance( GameObject.FindGameObjectWithTag("Player").transform.position, this.transform.position );

	
	}
}
