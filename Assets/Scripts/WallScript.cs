﻿using UnityEngine;
using System.Collections;

public class WallScript : MonoBehaviour {

    public GameObject Button;
    public bool reqSecondButton;
    public GameObject secondButton;
    public bool shrinkBoth;
    private float originalScaleX;
    private float originalScaleY;
    public bool openner = false;

    

	// Use this for initialization
	void Start () {
        originalScaleX = this.transform.localScale.x;
        originalScaleY = this.transform.localScale.y;
        if (openner)
        {
            this.gameObject.transform.localScale = new Vector2(originalScaleX, 0);
        }

	}
	
	// Update is called once per frame
	void Update () {
        if (openner)
        {
            if (Button.GetComponent<ButtonScript>().pressed && this.originalScaleY > this.transform.localScale.y)
            {
                //this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y + 0.01f);
                this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y + 0.01f);

                print("openning");
                print(this.gameObject.transform.localScale);
            }
        }
        else
        {
            if (!reqSecondButton)
            {
                if (Button.GetComponent<ButtonScript>().pressed)
                {
                    if (shrinkBoth)
                    {
                        if (this.gameObject.transform.localScale.x > 0)
                        {
                            this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x - 0.01f, this.gameObject.transform.localScale.y);
                        }
                    }
                    if (this.gameObject.transform.localScale.y > 0)
                    {
                        this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y - 0.01f);
                    }
                }
                else
                {
                    if (shrinkBoth)
                    {
                        if (this.gameObject.transform.localScale.x < originalScaleX)
                        {
                            this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x + 0.02f, this.gameObject.transform.localScale.y);

                        }
                    }
                    if (this.gameObject.transform.localScale.y < originalScaleY)
                    {
                        this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y + 0.02f);

                    }
                }
            }
            else
            {
                if (Button.GetComponent<ButtonScript>().pressed && secondButton.GetComponent<ButtonScript>().pressed)
                {
                    if (shrinkBoth)
                    {
                        if (this.gameObject.transform.localScale.x > 0)
                        {
                            this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x - 0.01f, this.gameObject.transform.localScale.y);
                        }
                    }
                    if (this.gameObject.transform.localScale.y > 0)
                    {
                        this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y - 0.01f);
                    }
                }
                else
                {
                    if (shrinkBoth)
                    {
                        if (this.gameObject.transform.localScale.x < originalScaleX)
                        {
                            this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x + 0.02f, this.gameObject.transform.localScale.y);

                        }
                    }
                    if (this.gameObject.transform.localScale.y < originalScaleY)
                    {
                        this.gameObject.transform.localScale = new Vector2(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y + 0.02f);

                    }
                }
            }
        }
         
	}
}
