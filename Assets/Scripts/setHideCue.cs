﻿using UnityEngine;
using System.Collections;

public class setHideCue : MonoBehaviour
{
    public bool hide = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            hide = true;
        }
    }

}