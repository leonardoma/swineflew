﻿using UnityEngine;
using System.Collections;

public class glowBehaviour : MonoBehaviour {

    public float timer = 0;
    float scaleDelta = 0;
    public float scaleMultiplier = 1;
    public float rotMultiplier = -1;

	// Use this for initialization
	void Start () {
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
        timer += 0.1f;
        //this.transform.localScale = new Vector2((timer % 60) / 120 + 0.5f, (timer % 60) / 120 + 0.5f);
        scaleDelta = Mathf.Sin(timer) * 0.05f;
        float scaleVal = 0.5f * scaleMultiplier + scaleDelta;
        this.transform.localScale = new Vector2(scaleVal, scaleVal);

        this.transform.eulerAngles = new Vector3(0, 0, timer * rotMultiplier);
	}
}
