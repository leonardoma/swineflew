﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PrototypeMessage : MonoBehaviour {

	public GameObject canvasObj, timerObj;

	private bool fade = false;
	private float blackAlpha = 0;

	public string nextLevelName;
	public float nextLevelPosX;
	public float nextLevelPosY;

	// Use this for initialization
	void Awake () {
		canvasObj.SetActive(false);
		if (Application.loadedLevelName != "duodenum")
		{
			PlayerPrefs.SetString("duobuttonpressed", ""); 
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			CycleLevel();
		}

		if (canvasObj.activeInHierarchy)
		{
			canvasObj.GetComponentInChildren<Image>().color = new Color(0, 0, 0, blackAlpha);
			blackAlpha += 0.05f;

			if (blackAlpha > 1.2f)
			{
				Application.LoadLevel(nextLevelName);
			}
		}

		if (Application.loadedLevelName == "test")
		{
			nextLevelPosX = 5.82f;
			nextLevelPosY = -23.47f;
		}

		if (Application.loadedLevelName == "stomach_sean")
		{
			nextLevelPosX = 3.13f;
			nextLevelPosY = -35.37f;
		}
	
	}

	void OnTriggerEnter2D(Collider2D col) {

		if (col.gameObject.tag == "Player")
		{
			CycleLevel();

			//timerObj.GetComponent<TimerScript>().alive = false;
		}

	}

	void CycleLevel() {

		canvasObj.SetActive(true);
		
		fade = true;
		
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			go.GetComponent<spriteMovement2>().enabled = false;
			go.GetComponent<Rigidbody2D>().isKinematic = true;
			go.GetComponent<Collider2D>().enabled = false;
		}

		PlayerPrefs.SetFloat ("lastX", nextLevelPosX);
		PlayerPrefs.SetFloat ("lastY", nextLevelPosY);
	}
}
