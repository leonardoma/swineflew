﻿using UnityEngine;
using System.Collections;

public class dumbWill : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-10000f, 10000f), Random.Range(-100f, 10000f)));

        if (Random.RandomRange(-1f, 1f) > 0.8f) {
            this.transform.localScale = new Vector3(1f, 1f, 1f); 
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Ground") {
            this.transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-10000f, 10000f), 10000f));
            //print("jumo");

			this.GetComponentInChildren<dumbWill_audio>().PlayJumpSound();
        }
    }
}
