﻿using UnityEngine;
using System.Collections;

public class AcidController : MonoBehaviour {
    public float killTime;
    private GameObject player;
    private GameObject dripAudio;
	// Use this for initialization 



	void Start () {
        dripAudio = Instantiate(Resources.Load("Audio/DripSound")) as GameObject;
        dripAudio.transform.parent = this.transform;
        Invoke("hide", killTime);
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void hide() {
        if (player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        float dist = Vector2.Distance(this.transform.position, player.transform.position);

        if (dist < 7f) {
            dripAudio.GetComponent<AudioSource>().Play();
        }
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);

        Invoke("kill", 1f);

    }

    void kill() {
        Destroy(this.gameObject);
    }
}
