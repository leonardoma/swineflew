﻿using UnityEngine;
using System.Collections;

public class CheckpointController : MonoBehaviour {


    private bool swapColour = false;
    private bool grumbled = false;
	public int id;

	GameObject checkpoint_audio;

	// Use this for initialization
	void Start () {

		checkpoint_audio = Instantiate( Resources.Load("Audio/checkpoint_fx")) as GameObject;

		checkpoint_audio.transform.parent = this.transform;
	
	}
	
	// Update is called once per frame
	void Update () {
        /*if (Input.GetKeyDown("space"))
        {
            t = 0;
        }

        if (t < 1)
        { // while t below the end limit...
            // increment it at the desired rate every update:
            t += Time.deltaTime / duration;
        }*/
        if (swapColour)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(this.gameObject.GetComponent<SpriteRenderer>().color, new Color(0.3f, 1f, 0.1f, 1), 0.1f);
            
            Component[] renderers = this.gameObject.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer childRenderer in renderers) {
                if (childRenderer.gameObject.tag != "Text") {
                    childRenderer.color = Color.Lerp(this.gameObject.GetComponent<SpriteRenderer>().color, new Color(0.6f, 1f, 0.4f, 1), 0.1f);

                }
            }
            //childRenderer.color = Color.Lerp(this.gameObject.GetComponent<SpriteRenderer>().color, new Color(0.6f, 1f, 0.4f, 1), 0.1f);
            

        }
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			PlayerPrefs.SetFloat("lastX", this.transform.position.x);
			PlayerPrefs.SetFloat("lastY", this.transform.position.y);

			print (this.transform.position.x + ":" + this.transform.position.y);
			print ("check point saved");

            Component[] textPopups = this.gameObject.GetComponentsInChildren<TextPopup>();
            foreach (TextPopup text in textPopups) {
                text.showText = true;
            }

			//play checkpoint audio first time touched
			if (!swapColour)
			{
				checkpoint_audio.GetComponent<AudioSource>().Play ();
			}

            swapColour = true;

			//Destroy (this.gameObject);
            
			if (GameObject.FindGameObjectsWithTag("Player").Length > 1 || col.gameObject.transform.localScale.x >0.5f && Application.loadedLevelName == "nasal") {
				print(2);
				PlayerPrefs.SetFloat("scale", 1f);
			}else {
				print(1);
				PlayerPrefs.SetFloat("scale", 0.5f);
			}

                        
		}

        if (!grumbled)
        {
            grumbled = true;

            GameObject[] grumblers = GameObject.FindGameObjectsWithTag("Grumblable");

            foreach (GameObject grumbler in grumblers)
            {
                grumbler.GetComponent<stomachgrumbler>().callGrumble(1f);
            }
        }
        
	}

}
