﻿using UnityEngine;
using System.Collections;

public class KeepPressed : MonoBehaviour {
    public ButtonScript btn;
	public CheckpointController cp;
	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetString("duobuttonpressed") == "open") {
            btn.pressed = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void OnTriggerEnter2D(Collider2D col) {
		print("cp id: " + cp.id);
		if (col.gameObject.tag == "Player" && cp.id == 1) {
            print("pressed");
            if (btn != null) {
                PlayerPrefs.SetString("duobuttonpressed", "open");

            } else {
                PlayerPrefs.SetString("duobuttonpressed", "checkpoint");

            }
            Destroy(this.gameObject);

        }
    }
}
