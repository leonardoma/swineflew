﻿using UnityEngine;
using System.Collections;

public class willSpewer : MonoBehaviour {
    private bool startSpew = false;
    private bool endSpew = false;
    private float willTimer;
    private float nextWill = 100f;

    private GameObject player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (startSpew && !endSpew) {
            if (willTimer >= nextWill) {
                nextWill = nextWill/1.2f;
                GameObject newWill = Instantiate(Resources.Load("Prefabs/dumbWill")) as GameObject;
                newWill.transform.position = this.transform.position;
                if (nextWill < 0.1f) {
                    endSpew = true;
					GameObject fadeCanvas = Instantiate ( Resources.Load("Prefabs/FadeBlack_Canvas")) as GameObject;
                    print("endSpew");
                }
                willTimer = 0f;
            }
            willTimer++;
        }
	}

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Player") {
            player = col.gameObject;
            startSpew = true;
        }
    }
}
