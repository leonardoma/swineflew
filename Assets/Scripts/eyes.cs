﻿using UnityEngine;
using System.Collections;

public class eyes : MonoBehaviour {
    public float horizMultiplier = 1f;
    public float vertMultiplier = 1f;
    public float rotMultiplier = 1f;

    private float tempHoriz;
    private float clampHoriz;

    private float tempVert;
    private float clampVert;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //float parentRot = this.transform.parent.localRotation.z;
        //Debug.Log(parentRot - this.transform.localRotation.z);
        //Debug.Log(this.transform.parent.parentRot);
        
        //transform.localRotation = Quaternion.Euler(0, 45, 30);
        transform.rotation = Quaternion.Euler(this.transform.parent.GetComponent<Rigidbody2D>().velocity.y * 5 * rotMultiplier, -this.transform.parent.GetComponent<Rigidbody2D>().velocity.x * 7 * rotMultiplier, -this.transform.parent.GetComponent<Rigidbody2D>().velocity.x * 3 * rotMultiplier);
        tempHoriz = Mathf.Clamp(this.transform.parent.GetComponent<Rigidbody2D>().velocity.x / 40, -0.8f, 0.8f);
        tempVert = Mathf.Clamp(this.transform.parent.GetComponent<Rigidbody2D>().velocity.y / 20, -0.2f, 0.3f);
        transform.position = new Vector2(this.transform.parent.GetComponent<Rigidbody2D>().position.x + tempHoriz * horizMultiplier, this.transform.parent.GetComponent<Rigidbody2D>().position.y + tempVert * vertMultiplier);

        //Debug.Log(this.transform.parent.GetComponent<Rigidbody2D>().velocity.x);
	}
}
