﻿using UnityEngine;
using System.Collections;

public class Player_sounds : MonoBehaviour {

	private float smallPitch;
	private float bigPitch;

	public AudioClip[] jumpSounds;
	public AudioClip[] painSounds;
	public AudioClip[] landingSounds;
	public AudioClip[] stickingSounds;

	public AudioClip splitSound, joinSound;

	private AudioSource audioSource;

	private bool dead = false;
	private bool jumped = false;

	private AudioSource effect_as;

	private bool alive = true;

	private float rightTriggerVal = 0;

	private spriteMovement2 sm;

	KeyCode jump_joystick, split_joystick, join_joystick, stick_joystick;

	// Use this for initialization
	void Start () {

		audioSource = this.GetComponent<AudioSource>();

		effect_as = this.transform.GetChild(0).GetComponent<AudioSource>();

		smallPitch = Random.Range (3, 5);

		bigPitch = Random.Range (-6, -4);


		audioSource.pitch = Mathf.Pow( 1.05946f, smallPitch );

		sm = this.transform.parent.GetComponent<spriteMovement2>();

		if (Input.GetJoystickNames ().Length > 0)
		{

			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				split_joystick = KeyCode.JoystickButton12;
				join_joystick = KeyCode.JoystickButton13;
				stick_joystick = KeyCode.JoystickButton9;
				
				print ("ps3 controller connected");
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				split_joystick = KeyCode.JoystickButton3;
				join_joystick = KeyCode.JoystickButton2;
				stick_joystick = KeyCode.JoystickButton7;
				
				print ("ps4 controller connected");
			}

		}

	
	}
	
	// Update is called once per frame
	void Update () {

		if (alive)
		{
			if ((Input.GetKeyDown (KeyCode.Space)) && ( sm.touchingGround || sm.touchingLeft || sm.touchingRight ))
			{
				Play_JumpAudio();
				jumped = true;
			}

			if ( ( sm.touchingGround || sm.touchingLeft || sm.touchingRight )  )
			{
				if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow))
				{
					if (Input.GetKey (KeyCode.C) )
					{
						Play_StickingAudio(0.5f);
					}
					else
					{
						Play_StickingAudio(0.2f);
					}

				}
			}
			else
			{
				effect_as.Stop();
			}

			if (Input.GetKeyUp(KeyCode.C))
			{
				jumped = true;
				effect_as.Stop();
			}

			if (Input.GetKeyUp (KeyCode.UpArrow) || Input.GetKeyUp (KeyCode.DownArrow) || Input.GetKeyUp (KeyCode.LeftArrow) || Input.GetKeyUp (KeyCode.RightArrow))
			{
				effect_as.Stop();
			}
		}

		if (Input.GetJoystickNames ().Length > 0) {
			JoyStickUpdate ();
		}
	
	}

	void JoyStickUpdate() {

//		if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
//		{
//			rightTriggerVal = Input.GetAxis("StickMac");
//		}
//		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
//		{
//			rightTriggerVal = Input.GetAxis("StickWin");
//		}

		//stiky trigger 
		if (Input.GetKey(stick_joystick))
		{
			rightTriggerVal = 1;
		}
		else
		{
			rightTriggerVal = 0;
		}

		if (alive)
		{
			if (Input.GetKeyDown(jump_joystick) && ( sm.touchingGround || sm.touchingLeft || sm.touchingRight ))
			{
				Play_JumpAudio();
				jumped = true;
			}
			
			if ( ( sm.touchingGround || sm.touchingLeft || sm.touchingRight )  )
			{
				if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
				{
					if (rightTriggerVal == 1)
					{
						Play_StickingAudio(0.5f);
					}
					else
					{
						Play_StickingAudio(0.2f);
					}
					
				}
			}
			else
			{
				effect_as.Stop();
			}
			
			if (rightTriggerVal == 0)
			{
				jumped = true;
				effect_as.Stop();
			}
			
			if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
			{
				effect_as.Stop();
			}
		}
	}

	public void Play_JumpAudio() {



		if (this.transform.parent.localScale.x > 0.5)
		{
			audioSource.pitch = Mathf.Pow( 1.05946f, bigPitch );
		}
		
		else
		{
			audioSource.pitch = Mathf.Pow( 1.05946f, smallPitch );
		}

		audioSource.volume = 0.5f - (0.1f * (GameObject.FindGameObjectsWithTag("Player").Length - 1) );

		audioSource.clip = jumpSounds[ Mathf.FloorToInt (Random.Range (0, jumpSounds.Length)) ];

		audioSource.time = 0;

		audioSource.Play ();

	}

	public void Play_LandingAudio()
	{
		if (jumped)
		{
			effect_as.pitch = 1f;
			
			effect_as.volume = 0.5f - (0.2f * (GameObject.FindGameObjectsWithTag("Player").Length - 1) );
			
			effect_as.clip = landingSounds[ Mathf.FloorToInt (Random.Range (0, landingSounds.Length)) ];
			
			effect_as.time = 0;
			effect_as.Play ();
		}

		jumped = false;
	}

	public void Play_StickingAudio(float initVol)
	{
		if (!effect_as.isPlaying)
		{
			effect_as.pitch = 1f;
			
			effect_as.volume = initVol - (0.2f * (GameObject.FindGameObjectsWithTag("Player").Length - 1) );
			
			effect_as.clip = stickingSounds[ Mathf.FloorToInt (Random.Range (0, stickingSounds.Length)) ];
			
			effect_as.time = 0;
			effect_as.Play ();
		}
	}

	public void Play_DeathAudio(float size) {

		if (alive)
		{
			if (size > 0.5f)
			{
				audioSource.pitch = Mathf.Pow( 1.05946f, bigPitch );
			}
			
			else
			{
				audioSource.pitch = Mathf.Pow( 1.05946f, smallPitch );
			}
			
			dead = true;
			
			audioSource.volume = 1.0f - (0.2f * (GameObject.FindGameObjectsWithTag("Player").Length - 1) );
			
			audioSource.clip = painSounds[ Mathf.FloorToInt (Random.Range (0, painSounds.Length)) ];
			
			audioSource.time = 0;
			
			audioSource.Play ();
			
			alive = false;
			
			StartCoroutine("Sound_Death");
		}


	}

	public void Play_SplitJoinAudio(string command)
	{
		audioSource.pitch = 1.0f;

		audioSource.volume = 0.5f - (0.1f * (GameObject.FindGameObjectsWithTag("Player").Length - 1) );

		if (command == "split")
		{
			audioSource.clip = splitSound;
		}
		if (command == "join")
		{
			audioSource.clip = joinSound;
		}
		
		audioSource.time = 0;
		
		audioSource.Play ();
	}

	IEnumerator Sound_Death()
	{
		yield return new WaitForSeconds(5.0f);

		Destroy(this.gameObject);
	}
}
