﻿using UnityEngine;
using System.Collections;

public class background_audio : MonoBehaviour {

	public AudioClip[] audioClips;

	AudioSource audioSource;

	float volume = 0;

	// Use this for initialization
	void Awake () {

		audioSource = this.GetComponent<AudioSource>();

		int soundNum = 0;

		switch(Application.loadedLevelName) {
			case "test":		soundNum = 0;
								volume = 0.2f;
								break;
			case "duodenum":	soundNum = 1;
								volume = 0.2f;
								break;
			case "stomach_sean":soundNum = 2;
								volume = 0.1f;
								GameObject stomachRumble = Instantiate( Resources.Load("Audio/Stomach_rumble") ) as GameObject;
								stomachRumble.GetComponent<AudioSource>().volume = 0.7f;
								stomachRumble.transform.parent = this.transform;
								break;
			case "nasal":		soundNum = 3;
								volume = 0.3f;
								break;
		}

		//Mathf.FloorToInt( Random.Range(0, audioClips.Length) )

		if (soundNum < audioClips.Length)
		{
			audioSource.clip = audioClips[ soundNum ];
		}
		else
		{
			audioSource.clip = audioClips[ 0 ];
		}

		audioSource.Play ();


		if (soundNum == PlayerPrefs.GetInt("prevSound"))
		{
			audioSource.time = PlayerPrefs.GetFloat("musicTime") + 0.033f;
		}
		else
		{
			PlayerPrefs.SetFloat("musicTime", 0);
			audioSource.time = 0;
		}

		audioSource.volume = volume;



		PlayerPrefs.SetInt("prevSound", soundNum);
	
	}
	
	// Update is called once per frame
	void Update () {

		PlayerPrefs.SetFloat("musicTime", audioSource.time);
	
	}
}
