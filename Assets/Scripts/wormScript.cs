﻿using UnityEngine;
using System.Collections;

public class wormScript : MonoBehaviour {

	private float attackingDistance = 3.0f;

	private Animator anim; 

	private float scaleVal;

	GameObject wormAudio;

	GameObject wormEyes;

	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator>();

		scaleVal = Mathf.Abs (this.transform.localScale.x);

        attackingDistance *= scaleVal;

		wormAudio = Instantiate( Resources.Load( "Audio/wormAudio" )) as GameObject;
		wormAudio.transform.parent = this.transform;

		wormEyes = this.transform.FindChild("worm_eyes").gameObject;
	
	}
	
	// Update is called once per frame
	void Update () {

		int playerIndex = 0;
		float closestDistance = 0;
		float playerDirection = 0;

		//distance to the closest player GameObject from this worm
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (playerIndex == 0)
			{
				closestDistance = Vector2.Distance(this.transform.position, go.transform.position);
				playerDirection = this.transform.position.x - go.transform.position.x;
			}

			else
			{
				float thisDistance = Vector2.Distance(this.transform.position, go.transform.position);

				if (closestDistance > thisDistance)
				{
					closestDistance = thisDistance;
					playerDirection = this.transform.position.x - go.transform.position.x;
				}
			}
			playerIndex ++;
		}

		//close distance. trigger near animation
		if (closestDistance < attackingDistance)
		{
			anim.SetBool("playerNear", true);


		}
		//far distance. trigger idle animation
		else
		{
			anim.SetBool("playerNear", false);

		}

		if (closestDistance < attackingDistance * 2)
		{
			if (wormEyes.transform.localPosition.y > 1.0f)
			{
				if (!wormAudio.GetComponent<AudioSource>().isPlaying)
				{
					wormAudio.GetComponent<AudioSource>().time = 0;
					wormAudio.GetComponent<AudioSource>().Play ();
				}
			}
		}




		//turn to face player
		if (playerDirection > 0)
		{
			this.transform.localScale = new Vector3(-scaleVal, this.transform.localScale.y, 1); 
		}
		else
		{
			this.transform.localScale = new Vector3(scaleVal, this.transform.localScale.y, 1); 
		}

	
	}
}
