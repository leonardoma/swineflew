﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerScript : MonoBehaviour {

	public float timer = 0;

	public bool alive = true;

	public GameObject textObj;

	// Use this for initialization
	void Start () {
		alive = true;

		timer = PlayerPrefs.GetFloat("timer");
	}
	
	// Update is called once per frame
	void Update () {

		if (alive)
		{
			timer += Time.deltaTime;

			PlayerPrefs.SetFloat("timer", timer);

			textObj.GetComponent<Text>().text = timer.ToString("0.00");
		}

		if (Input.GetKeyDown(KeyCode.T))
		{
			textObj.SetActive( !textObj.activeSelf );
		}

	
	}
}
