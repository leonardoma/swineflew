﻿using UnityEngine;
using System.Collections;

public class jumpCollider : MonoBehaviour {

    public bool leftWall = false;
    public bool rightWall = false;
    public bool touchingGround = false;
    public bool touchingLeft = false;
    public bool touchingRight = false;


    public GameObject following = null;

	// Use this for initialization
	void Start () {
        //following = GameObject.FindGameObjectsWithTag("Player")[GameObject.FindGameObjectsWithTag("Player").Length - 1];
		following = this.transform.parent.gameObject;

	}
	
	// Update is called once per frame
	void Update () {
        if (leftWall)
        {
            if (following.GetComponent<spriteMovement2>().isLarge)
            {
                this.transform.position = new Vector2(following.GetComponent<Rigidbody2D>().transform.position.x - 0.1f, following.GetComponent<Rigidbody2D>().transform.position.y);
            }
            else
            {
                this.transform.position = new Vector2(following.GetComponent<Rigidbody2D>().transform.position.x - 0.1f, following.GetComponent<Rigidbody2D>().transform.position.y);
            }
        }
        else if (rightWall)
        {
            if (following.GetComponent<spriteMovement2>().isLarge)
            {
                this.transform.position = new Vector2(following.GetComponent<Rigidbody2D>().transform.position.x + 0.1f, following.GetComponent<Rigidbody2D>().transform.position.y);
            }
            else
            {
                this.transform.position = new Vector2(following.GetComponent<Rigidbody2D>().transform.position.x + 0.1f, following.GetComponent<Rigidbody2D>().transform.position.y);
            }
        }
        else
        {
            if (following.GetComponent<spriteMovement2>().isLarge)
            {
                this.transform.position = new Vector2(following.GetComponent<Rigidbody2D>().transform.position.x, following.GetComponent<Rigidbody2D>().transform.position.y - 0.5f);
            }
            else
            {
                this.transform.position = new Vector2(following.GetComponent<Rigidbody2D>().transform.position.x, following.GetComponent<Rigidbody2D>().transform.position.y - 0.3f);
            }
        }
        
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "Large Object")
        {
            if (leftWall)
            {
                following.GetComponent<spriteMovement2>().touchingLeft = true;
                touchingLeft = true;
            }
            else if (rightWall)
            {
                following.GetComponent<spriteMovement2>().touchingRight = true;
                touchingRight = true;
            }
            else
            {
                following.GetComponent<spriteMovement2>().touchingGround = true;
                touchingGround = true;
            }
            
            //Debug.Log(following.GetComponent<spriteMovement2>().touchingGround);

           
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "Large Object")
        {
            if (leftWall)
            {
                following.GetComponent<spriteMovement2>().touchingLeft = false;
                touchingLeft = false;
            }
            else if (rightWall)
            {
                following.GetComponent<spriteMovement2>().touchingRight = false;
                touchingRight = false;
            }
            else
            {
                following.GetComponent<spriteMovement2>().touchingGround = false;
                touchingGround = false;
            }
            
            //Debug.Log(following.GetComponent<spriteMovement2>().touchingGround);

        }
    }


}
