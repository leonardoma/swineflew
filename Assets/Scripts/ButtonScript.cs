﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {
// Seb'changes
    public bool upsideDown = false;
    public bool pressed = false;
    public bool pressing = false;
    public bool reqLargeWill = false;
    public bool reqLargeObject = false;
    public bool lockPressed = false;
    public bool reqSecondButton = false;
    public GameObject secondButton;

	private CameraScript cam;

    private float origY;
    private Color origColour;
    private bool shrinkIt = true;
	
	public bool cameraZoom = false;
	public Vector2 cameraZoomPosition;
	public float cameraOrthoSize;

	// Use this for initialization
	void Start () {
	   //SEAN EDIT MWEIPWEFPOJWEF
        origY = this.transform.position.y;
        origColour = this.gameObject.GetComponent<SpriteRenderer>().color;

		cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>();
	}
	
	// Update is called once per frame
	void Update () {

        if (pressed) {

			if (cameraZoom)
			{
				//position, camera  orthographic size, zoom in time, stay time, zoom out time
				cam.StartFeedbackZoom(cameraZoomPosition, cameraOrthoSize, 0.5f, 2, 2.5f);
				cameraZoom = false;
			}

            this.gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(this.gameObject.GetComponent<SpriteRenderer>().color, new Color(origColour.r + 0.4f, origColour.g + 0.4f, origColour.b + 0.4f, 1), 0.1f);
            if (upsideDown) {
                this.transform.position = new Vector3(this.transform.position.x, origY + 0.1f, 0f);
            } else {
                this.transform.position = new Vector3(this.transform.position.x, origY - 0.1f, 0f);
            }
            if (shrinkIt) {
                shrinkIt = false;
                //StartCoroutine(WaitShrink());
            }
        } else if (pressing) {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(this.gameObject.GetComponent<SpriteRenderer>().color, new Color(origColour.r + 0.2f, origColour.g + 0.2f, origColour.b + 0.2f, 1), 0.1f);
            if (!reqSecondButton) {
                pressed = true;
                if (upsideDown) {
                    this.transform.position = new Vector3(this.transform.position.x, origY + 0.1f, 0f);
                } else {
                    this.transform.position = new Vector3(this.transform.position.x, origY - 0.1f, 0f);
                }
            } else {
                if (secondButton.GetComponent<ButtonScript>().pressing) {
                    pressed = true;
                    if (upsideDown) {
                        this.transform.position = new Vector3(this.transform.position.x, origY + 0.1f, 0f);
                    } else {
                        this.transform.position = new Vector3(this.transform.position.x, origY - 0.1f, 0f);
                    }
                }
            }     
        } else {
            this.transform.position = new Vector3(this.transform.position.x, origY, 0f);
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(this.gameObject.GetComponent<SpriteRenderer>().color, origColour, 0.1f);

        }
        
	}


    void OnCollisionEnter2D(Collision2D col)
    {
        print(col.gameObject.tag);
        if ((!upsideDown && col.gameObject.transform.position.y > this.transform.position.y) || (upsideDown && col.gameObject.transform.position.y < this.transform.position.y)) {
            if (reqLargeWill) {
                if (col.gameObject.tag == "Player" && col.gameObject.transform.localScale.x > 0.5) {
                    pressing = true;

                }
            } else if (reqLargeObject) {
                if (col.gameObject.tag == "Large Object") { 

                    pressing = true;

                }
            } else {
                if (col.gameObject.tag == "Player") {
                    pressing = true;
                    //print("pre"); 
                }
            }
        }   
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            pressing = false; 
        }
    }

    IEnumerator ButtonUp() {
        print("btn");
        yield return new WaitForSeconds(0.1f);
        pressing = false;

    }

    IEnumerator WaitShrink() {
        yield return new WaitForSeconds(1f);
        LerpScale(1f);
    }

    void LerpScale(float time)
    {
        print("shrink");
        Vector3 originalScale = transform.localScale;
        Vector3 targetScale = new Vector3(0f, 0f, 0f);
        float originalTime = time;


        while (time > 0.0f)
        {
            print("shrinking");
           time -= Time.deltaTime;
        
        
            transform.localScale = Vector3.Lerp(targetScale, originalScale, time / originalTime);

        
        }
    }
}
