﻿using UnityEngine;
using System.Collections;

public class stomach_food_audioSource : MonoBehaviour {

	public AudioClip[] clips;

	// Use this for initialization
	void Start () {

		this.GetComponent<AudioSource>().clip = clips[ Mathf.FloorToInt( Random.Range (0, clips.Length) ) ];
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
