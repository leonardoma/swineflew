﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelperScript : MonoBehaviour {

	public string message;
	
	public Image speechBoxObj;
	public Text textObj;

	public float speechBoxDist;

	// Use this for initialization
	void Start () {
		textObj.text = message;

		textObj.color = new Color(0, 0, 0, 0);
		speechBoxObj.color = new Color(1, 1, 1, 0);
	}
	
	// Update is called once per frame
	void Update () {

		//get distance to closest player
		int playerIndex = 0;
		float closestDistance = 0;

		//distance to the closest player GameObject from this worm
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (playerIndex == 0)
			{
				closestDistance = Vector2.Distance(this.transform.position, go.transform.position);
			}
			
			else
			{
				float thisDistance = Vector2.Distance(this.transform.position, go.transform.position);
				
				if (closestDistance > thisDistance)
				{
					closestDistance = thisDistance;
				}
			}
			playerIndex ++;
		}

		if (closestDistance < speechBoxDist)
		{
			float alphaValue = (speechBoxDist - closestDistance) * 1.25f / (speechBoxDist);

			textObj.color = new Color(0, 0, 0, alphaValue);
			speechBoxObj.color = new Color(1, 1, 1, alphaValue);
		}
			
	}
}
