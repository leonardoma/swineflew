﻿using UnityEngine;
using System.Collections;

public class joinWills : MonoBehaviour {

	KeyCode jump_joystick, split_joystick, join_joystick, stick_joystick;

	// Use this for initialization
	void Start () {

		if (Input.GetJoystickNames ().Length > 0)
		{

			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				split_joystick = KeyCode.JoystickButton12;
				join_joystick = KeyCode.JoystickButton13;
				stick_joystick = KeyCode.JoystickButton9;
				
				print ("ps3 controller connected");
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				split_joystick = KeyCode.JoystickButton3;
				join_joystick = KeyCode.JoystickButton2;
				stick_joystick = KeyCode.JoystickButton7;
				
				print ("ps4 controller connected");
			}

		}
	
	}
	
	// Update is called once per frame
	void Update () {

		bool joined = false;

		if (Input.GetKeyDown(KeyCode.B) || Input.GetKeyDown(join_joystick) )
        {
            foreach (GameObject origWill in GameObject.FindGameObjectsWithTag("Player"))
            {
                int willCount = 0;
                foreach (GameObject will in GameObject.FindGameObjectsWithTag("Player"))
                {
                    willCount++;
                    if (will != origWill && Vector2.Distance(will.transform.position, origWill.transform.position) < 1 && (origWill.transform.localScale.x != 1.0f && will.gameObject.transform.localScale.x != 1.0f))
                    {
                        Destroy(origWill.gameObject);
                        will.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

                        will.GetComponent<spriteMovement2>().isLarge = true;
                        will.GetComponent<spriteMovement2>().vertMove = 6000;
						joined = true;
						will.GetComponentInChildren<Player_sounds>().Play_SplitJoinAudio("join");
                        break;
                    }
                }

				if (joined)
				{
					break;
				}
               
            }


        }
	}
}
