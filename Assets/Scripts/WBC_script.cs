﻿using UnityEngine;
using System.Collections;

public class WBC_script : MonoBehaviour {

    public bool alternateGround = false;

	private GameObject player;
	public GameObject eyes;

	private Vector2 groundContactPoint;

	private float speed;

	public bool following;

	private float horizontalSpeedCap, upSpeedCap, downSpeedCap;

	private bool movingRight = true;
	private bool dirFlipped = false;

	private Vector2 centerPointPath;
	public float maxDistanceFromCenter;

	GameObject audio;

	// Use this for initialization
	void Start () {
		speed = 60f;
		upSpeedCap = 2f;
		downSpeedCap = -2.0f;
		horizontalSpeedCap = 0.5f;

		centerPointPath = this.transform.position;

		following = true;

		GetComponent<Rigidbody2D>().gravityScale = 0;

		audio = Instantiate ( Resources.Load ("Audio/WBC_noise")) as GameObject;
		audio.transform.parent = this.transform;

		audio.GetComponent<AudioSource>().time = Random.Range( 0f, audio.GetComponent<AudioSource>().clip.length );
	
	}
	
	// Update is called once per frame
	void Update () {

		audio.transform.position = this.transform.position;

		ChangeDirection();

		//lockRotationOnEyes
		eyes.transform.eulerAngles = new Vector3(0, 0, 0);
	}

	void FixedUpdate()
	{
		Vector2 gravityDir = groundContactPoint - new Vector2(this.transform.position.x, this.transform.position.y);
		gravityDir = gravityDir.normalized;
		Vector3 forward = Vector3.Cross(gravityDir, Vector3.forward);

		GetComponent<Rigidbody2D>().AddForce ( gravityDir * 150f );



		if (movingRight)
		{
			GetComponent<Rigidbody2D>().AddForce( 40 * forward );
		}

		else
		{
			GetComponent<Rigidbody2D>().AddForce( -40 * forward );
		}

		float distance = Vector2.Distance(this.transform.position, centerPointPath);
		
		if (distance > maxDistanceFromCenter)
		{
//			if (movingRight)
//			{
//				GetComponent<Rigidbody2D>().AddForce( -80 * forward );
//			}
//			else
//			{
//				GetComponent<Rigidbody2D>().AddForce( 80 * forward );
//			}
			if (!dirFlipped)
			{
				movingRight = !movingRight;
				dirFlipped = true;
			}
		}
		else
		{
			dirFlipped = false;
		}

		//horiz velocity cap
		if (this.GetComponent<Rigidbody2D>().velocity.x > horizontalSpeedCap)
		{
			this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalSpeedCap, this.GetComponent<Rigidbody2D>().velocity.y);
		}
		if (this.GetComponent<Rigidbody2D>().velocity.x < -horizontalSpeedCap)
		{
			this.GetComponent<Rigidbody2D>().velocity = new Vector2(-horizontalSpeedCap, this.GetComponent<Rigidbody2D>().velocity.y);
		}

		//vert velocity cap
		if (this.GetComponent<Rigidbody2D>().velocity.y > upSpeedCap)
		{
			this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, upSpeedCap);
		}
		if (this.GetComponent<Rigidbody2D>().velocity.y < downSpeedCap)
		{
			this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, downSpeedCap);
		}

	}

	void ChangeDirection()
	{

	}

	void FollowWill()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		
		float dist = Vector2.Distance(this.transform.position, player.transform.position);
		
		
		if (dist < 7 && following)
		{
			Vector2 movementVec = player.transform.position - this.transform.position;
			movementVec = movementVec.normalized;
			
			this.GetComponent<Rigidbody2D>().AddForce(new Vector2(movementVec.x * speed, 0));
		}
	}

	public void Eating()
	{
		following = false;
		this.GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 0.3f);

		this.gameObject.tag = "Untagged";
	}

	void OnCollisionEnter2D(Collision2D col)
	{
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "AlternateGround") 
		{
			foreach(ContactPoint2D contactPoint in col.contacts)
			{
				//print (	contactPoint.point);
				
				groundContactPoint = contactPoint.point;
			}
		}
	}

	void OnCollisionStay2D(Collision2D col)
	{
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "AlternateGround")
		{
			foreach(ContactPoint2D contactPoint in col.contacts)
			{
				//print (	contactPoint.point);
				
				groundContactPoint = contactPoint.point;
			}
		}
	}
}
