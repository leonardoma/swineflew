﻿using UnityEngine;
using System.Collections;

public class uvulasound : MonoBehaviour {
    private GameObject uvulaAudio;
	// Use this for initialization
	void Start () {
        uvulaAudio = Instantiate(Resources.Load("Audio/UvulaBump")) as GameObject;
        uvulaAudio.transform.parent = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D (Collision2D col) {
        if (col.gameObject.tag == "Player") {
            uvulaAudio.GetComponent<AudioSource>().Play();
            print("bump");
        }
    }
}
