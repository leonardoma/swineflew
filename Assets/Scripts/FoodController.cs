﻿using UnityEngine;
using System.Collections;

public class FoodController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("kill", 1.0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void kill()
    {
        //Debug.Log(this.GetComponent<Rigidbody2D>().velocity.magnitude);
        if (this.GetComponent<Rigidbody2D>().velocity.magnitude < 1)
        {
            Destroy(this.gameObject);
        }
    }
}
