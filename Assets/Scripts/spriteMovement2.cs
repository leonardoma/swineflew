﻿using UnityEngine;
using System.Collections;

public class spriteMovement2 : MonoBehaviour
{
	
	public float horizMove, vertMove;
	private Vector2 groundContactPoint;
	public Sprite stickSprite;
	public Sprite rollSprite;
	public bool isLarge = false;
	public bool touchingGround = false;
	public bool touchingLeft = false;
	public bool touchingRight = false;
	private bool touchingSurface = false;
	public bool movingRight = true;
	public bool movingUp = true;
	public float horizontalSpeedCap;
	public float lastDirectionWhileKeypressed;
	
	//public jumpCollider uniqueJumpCollider;
	public GameObject uniqueJumpCollider;
	public GameObject uniqueJumpColliderLeft;
	public GameObject uniqueJumpColliderRight;
	public GameObject particleSys;
	public GameObject playerAudio;
	public GameObject arrow;
	private Vector2 gravityDir;
	private float rightTriggerVal = 0;

	KeyCode jump_joystick, split_joystick, join_joystick, stick_joystick;
	
	// Use this for initialization
	void Awake ()
	{
		// prevent getting large WILL in the transition from level 4 to level 1
		if (Application.loadedLevelName == "test") {
			PlayerPrefs.SetFloat("scale", 0.5f);
			this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		}
		
		//if first player character, load from check point
        if (GameObject.FindGameObjectsWithTag("Player").Length == 1 && (Application.loadedLevelName == "test" || Application.loadedLevelName == "duodenum" || Application.loadedLevelName == "nasal")) {
			//first time play
			if (PlayerPrefs.GetInt ("PlayCount") == 0 && Application.loadedLevelName == "test") {
				PlayerPrefs.SetFloat ("lastX", 0.8f);
				PlayerPrefs.SetFloat ("lastY", -20.5f);
				PlayerPrefs.SetFloat("scale", 0.5f);
				this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			}
			if (PlayerPrefs.GetInt ("PlayCount") == 0 && Application.loadedLevelName == "duodenum")
			{
				PlayerPrefs.SetFloat ("lastX", 5.8f);
				PlayerPrefs.SetFloat ("lastY", -23.62f);
				PlayerPrefs.SetFloat("scale", 0.5f);
				this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			}

            if (PlayerPrefs.GetInt("PlayCount") == 0 && Application.loadedLevelName == "nasal") {
                PlayerPrefs.SetFloat("lastX", 3.13f);
                PlayerPrefs.SetFloat("lastY", -35.37f);
				PlayerPrefs.SetFloat("scale", 0.5f);
				this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }

			print ("Play number: " + PlayerPrefs.GetInt ("PlayCount"));
			this.transform.position = new Vector3 (PlayerPrefs.GetFloat ("lastX"), PlayerPrefs.GetFloat ("lastY"), 0);
			PlayerPrefs.SetInt ("PlayCount", (PlayerPrefs.GetInt ("PlayCount") + 1));
		}
		
		GetComponent<Rigidbody2D> ().mass = 10;
		GetComponent<Rigidbody2D> ().drag = 5;
		GetComponent<Rigidbody2D> ().angularDrag = 10;
		GetComponent<Rigidbody2D> ().gravityScale = 3;
		
		horizMove = 800;
		vertMove = 4500;

		if (PlayerPrefs.GetFloat("scale") == 0){
			PlayerPrefs.SetFloat("scale", 0.5f);
		}

		float scale = PlayerPrefs.GetFloat ("scale");

		if (GameObject.FindGameObjectsWithTag ("Player").Length == 1) {
			this.transform.localScale = new Vector3 (scale, scale, scale);
		}
		
		//Instantiate(uniqueJumpCollider,)
		
		uniqueJumpCollider = Instantiate (Resources.Load ("Prefabs/jumpCollider")) as GameObject;
		uniqueJumpCollider.transform.parent = this.transform;
		
		uniqueJumpColliderLeft = Instantiate (Resources.Load ("Prefabs/jumpCollider")) as GameObject;
		uniqueJumpColliderLeft.transform.parent = this.transform;
		uniqueJumpColliderLeft.gameObject.GetComponent<jumpCollider> ().leftWall = true;
		
		uniqueJumpColliderRight = Instantiate (Resources.Load ("Prefabs/jumpCollider")) as GameObject;
		uniqueJumpColliderRight.transform.parent = this.transform;
		uniqueJumpColliderRight.gameObject.GetComponent<jumpCollider> ().rightWall = true;
		
		particleSys.transform.gameObject.SetActive (false);
		//Debug.Log(uniqueJumpCollider);


		//controller set up
		if (Input.GetJoystickNames ().Length > 0)
		{
			//ps3
			if (Input.GetJoystickNames ()[0].Contains("Sony PLAYSTATION(R)3 Controller"))
			{
				jump_joystick = KeyCode.JoystickButton14; 
				split_joystick = KeyCode.JoystickButton12;
				join_joystick = KeyCode.JoystickButton13;
				stick_joystick = KeyCode.JoystickButton9;
				
				print ("ps3 controller connected");
			}
			
			//ps4
			if (Input.GetJoystickNames ()[0].Contains("Wireless Controller"))
			{
				jump_joystick = KeyCode.JoystickButton1; 
				split_joystick = KeyCode.JoystickButton3;
				join_joystick = KeyCode.JoystickButton2;
				stick_joystick = KeyCode.JoystickButton7;
				
				print ("ps4 controller connected");
			}
		}



	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (Input.GetKeyDown (KeyCode.V) && this.transform.localScale.x > 0.7f) {
			print ("v pressed");
			playerAudio.GetComponent<Player_sounds>().Play_SplitJoinAudio("split");
//			PlayerPrefs.SetFloat("scale", 0.5f);
			this.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
			isLarge = false;
			vertMove = 4500;
			GameObject clone = Instantiate (this.gameObject) as GameObject;
			clone.transform.name = "Will";
			Debug.Log (clone);
		}
		
		if (Input.GetKeyDown (KeyCode.Space) && (touchingGround /*|| this.touchingSurface*/)) {
			//ForceChar (0, vertMove);
			//touchingGround = false;
			//GetComponent<Rigidbody2D>().AddForce ( new Vector2(0,vertMove*2) );
			
			//Debug.Log(jumpVec);
			Vector2 jumpVector = new Vector2 (0, vertMove * 2);
			
			//sticking
			if (Input.GetKey (KeyCode.C)) {
				//touching both left right
				if (touchingLeft && touchingRight) {
					GetComponent<Rigidbody2D> ().AddForce (jumpVector);
				}
				
				//touching left
				if ((touchingLeft && !touchingRight) || (!touchingLeft && touchingRight)) {
					GetComponent<Rigidbody2D> ().AddForce (jumpVector * 0.65f);
				}
			}
			//not sticking
			else {
				GetComponent<Rigidbody2D> ().AddForce (jumpVector);
			}
			
			
			
			
		}
		
		if (Input.GetKeyDown (KeyCode.Space) && !Input.GetKeyDown (KeyCode.LeftArrow) && touchingLeft && !touchingRight) {
			//ForceChar (0, vertMove);
			//touchingGround = false;
			//GetComponent<Rigidbody2D>().AddForce(new Vector2(horizMove * 10, 0));
			if (Input.GetKey (KeyCode.C)) {
				GetComponent<Rigidbody2D> ().AddForce (new Vector2 (horizMove * 16, 0));
			} else {
				GetComponent<Rigidbody2D> ().AddForce (new Vector2 (horizMove * 7, 0));
			}
			Debug.Log ("left");
		}
		
		if (Input.GetKeyDown (KeyCode.Space) && !Input.GetKeyDown (KeyCode.RightArrow) && touchingRight && !touchingLeft) {
			//ForceChar (0, vertMove);
			//touchingGround = false;
			//GetComponent<Rigidbody2D>().AddForce(new Vector2(-(horizMove * 10), 0));
			if (Input.GetKey (KeyCode.C)) {
				GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-horizMove * 16, 0));
			} else {
				GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-horizMove * 7, 0));
			}
			Debug.Log ("right");
		}
		
		//horiz velocity cap for moving
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow)) {
			if (this.GetComponent<Rigidbody2D> ().velocity.x > horizontalSpeedCap) {
				this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (horizontalSpeedCap, this.GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (this.GetComponent<Rigidbody2D> ().velocity.x < -horizontalSpeedCap) {
				this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-horizontalSpeedCap, this.GetComponent<Rigidbody2D> ().velocity.y);
			}
		}
		
		//print (GetComponent<Rigidbody2D>().velocity);
		
		//restart
		if (Input.GetKeyDown (KeyCode.R)) {

			if (Application.loadedLevelName == "test") {
				PlayerPrefs.SetFloat ("lastX", 0.8f);
				PlayerPrefs.SetFloat ("lastY", -20.5f);
			}
			if (Application.loadedLevelName == "duodenum")
			{
				PlayerPrefs.SetFloat ("lastX", 5.8f);
				PlayerPrefs.SetFloat ("lastY", -23.62f);
			}

            PlayerPrefs.SetString("duobuttonpressed", ""); 
			Application.LoadLevel (Application.loadedLevel);
			
			PlayerPrefs.SetFloat ("timer", 0);
		}
		
		//delete all playerPrefs
		if (Input.GetKeyDown (KeyCode.E)) {
			PlayerPrefs.DeleteAll ();
		}
		
		if (Input.GetJoystickNames ().Length > 0) {
			JoyStickRegularUpdate ();
		}
	}
	
	void FixedUpdate ()
	{
		//Debug.Log(gameObject.GetComponentInChildren<jumpCollider>().touchingGround);
		gravityDir = groundContactPoint - new Vector2 (this.transform.position.x, this.transform.position.y);
//		gravityDir = gravityDir.normalized;
		//Vector3 forward = Vector3.Cross(gravityDir, Vector3.forward);
		
		float angle = Vector2.Angle (groundContactPoint, this.transform.position);



		arrow.transform.eulerAngles = new Vector3 (0, 0, angle);
		
		//keyboard
		if (Input.GetKey (KeyCode.LeftArrow) && (Input.GetKey (KeyCode.C) && !touchingSurface || !Input.GetKey (KeyCode.C))) {
			ForceChar (-horizMove, 0);
			movingRight = false;
		}
		
		if (Input.GetKey (KeyCode.RightArrow) && (Input.GetKey (KeyCode.C) && !touchingSurface || !Input.GetKey (KeyCode.C))) {
			ForceChar (horizMove, 0);
			movingRight = true;
		}
		
		//jumpCollider jumpTest = uniqueJumpCollider;
		//GameObject.FindGameObjectWithTag("JumpCollider").GetComponent<jumpCollider>();
		if (Input.GetKey (KeyCode.C)) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = stickSprite;
		} else {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = rollSprite;
		}
		
		//sticking
		if (Input.GetKey (KeyCode.C) && touchingSurface) {
			//Debug.Log(this.groundContactPoint);
			this.GetComponent<Rigidbody2D> ().gravityScale = 0;
			
			//GetComponent<Rigidbody2D>().AddForce ( -Physics2D.gravity );
			//playtests were done with gravityDir * 2000;
			float gravityDirAmplifier = 4000f;

			float damp = 0.7f;
			
			
			if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow) )
			{


				if (Application.loadedLevelName == "test") {
					gravityDirAmplifier = 10000f;
					damp = 1.5f; 
				}

				if (Application.loadedLevelName == "duodenum"){
					gravityDirAmplifier = 10000f;				
				}

				if (Application.loadedLevelName == "stomach_sean") {
					gravityDirAmplifier = 8000f;
				}

				if (Application.loadedLevelName == "nasal") {
					gravityDirAmplifier = 9000;
					damp = 1.0f; 
				}
			}



			GetComponent<Rigidbody2D> ().AddForce (gravityDir * gravityDirAmplifier);


			// kinnda optional now
//			transform.position = groundContactPoint - new Vector2 (gravityDir.x, gravityDir.y);




			// two button support

			if ((Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.RightArrow))
				|| (Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.LeftArrow))) {
				ForceChar (0, horizMove * damp * 3 / 4);
				movingUp = true;
				if (Input.GetKey (KeyCode.LeftArrow)) {
					ForceChar (-horizMove * damp * 3 / 4, 0);
					movingRight = false;

				}
				if (Input.GetKey (KeyCode.RightArrow)) {
					ForceChar (horizMove * damp * 3 / 4, 0);
					movingRight = true;
				}

			} else if ((Input.GetKey (KeyCode.DownArrow) && Input.GetKey (KeyCode.RightArrow))
				|| (Input.GetKey (KeyCode.DownArrow) && Input.GetKey (KeyCode.LeftArrow))) {
				ForceChar (0, -horizMove * damp * 3 / 4);
				movingUp = false;
				if (Input.GetKey (KeyCode.LeftArrow)) {
					ForceChar (-horizMove * damp * 3 / 4, 0);
					movingRight = false;
				}
				if (Input.GetKey (KeyCode.RightArrow)) {
					ForceChar (horizMove * damp * 3 / 4, 0);

					movingRight = true;
				}
				
			} 
			// one button support
			else if (Input.GetKey (KeyCode.UpArrow) && !Input.GetKey (KeyCode.DownArrow)) {
				ForceChar (0, horizMove * damp);
				movingUp = true;
			} else if (Input.GetKey (KeyCode.DownArrow) && !Input.GetKey (KeyCode.UpArrow)) {
				ForceChar (0, -horizMove * damp);
				movingUp = false;
			} else if (Input.GetKey (KeyCode.LeftArrow) && !Input.GetKey (KeyCode.RightArrow)) {
				ForceChar (-horizMove * damp, 0);
				movingRight = false;
			} else if (Input.GetKey (KeyCode.RightArrow) && !Input.GetKey (KeyCode.LeftArrow)) {
//				if (gravityDir.x * gravityDirAmplifier < 0 && gravityDir.y * gravityDirAmplifier < 0) {  
//					ForceChar (horizMove * damp, -horizMove* damp);
//				}
//				else if (gravityDir.x * gravityDirAmplifier < 0 && gravityDir.y * gravityDirAmplifier > 0) {  
//					ForceChar (horizMove * damp, horizMove* damp);
//				}
//				else if (gravityDir.x * gravityDirAmplifier > 0 && gravityDir.y * gravityDirAmplifier > 0) {  
//					ForceChar (horizMove * damp, -horizMove* damp);
//				}
//				else if (gravityDir.x * gravityDirAmplifier > 0 && gravityDir.y * gravityDirAmplifier < 0) {  
//					ForceChar (horizMove * damp, horizMove* damp);
//				}

				ForceChar (horizMove * damp, 0);
				movingRight = true;
			}

			
			float verticalSpeedCap = horizontalSpeedCap * 0.75f;
			
			//vertical velocity cap for moving
			if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.DownArrow)) {
				if (this.GetComponent<Rigidbody2D> ().velocity.y > verticalSpeedCap) {
					this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (this.GetComponent<Rigidbody2D> ().velocity.x, verticalSpeedCap);
				}
				if (this.GetComponent<Rigidbody2D> ().velocity.y < -verticalSpeedCap) {
					this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (this.GetComponent<Rigidbody2D> ().velocity.x, -verticalSpeedCap);
				}
			}
			
		}
		
		if (Input.GetKeyUp (KeyCode.C) || !touchingSurface) {   
			this.GetComponent<Rigidbody2D> ().gravityScale = 3;
		}
		
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow)) {
			lastDirectionWhileKeypressed = GetComponent<Rigidbody2D> ().velocity.x;
		}
		
		
		if (Input.GetJoystickNames ().Length > 0) {
			JoyStickFixedUpdate ();
		}
		
	}
	
	void JoyStickFixedUpdate()
	{
//		if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
//		{
//			rightTriggerVal = Input.GetAxis("StickMac");
//		}
//		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
//		{
//			rightTriggerVal = Input.GetAxis("StickWin");
//		}

		//stiky trigger 
		if (Input.GetKey(stick_joystick))
		{
			rightTriggerVal = 1;
		}
		else
		{
			rightTriggerVal = 0;
		}

		if (rightTriggerVal == 1)
		{
			this.gameObject.GetComponent<SpriteRenderer>().sprite = stickSprite;
		}
		else
		{
			this.gameObject.GetComponent<SpriteRenderer>().sprite = rollSprite;
		}


		//joystick
		if ( Mathf.Abs (Input.GetAxis("Horizontal")) > 0 && ((rightTriggerVal == 1) && !touchingSurface) || rightTriggerVal != 1)
		{
			ForceChar (horizMove * Input.GetAxis ("Horizontal"), 0);
		}

		if (rightTriggerVal == 1)
		{
			this.gameObject.GetComponent<SpriteRenderer>().sprite = stickSprite;
		}
		else
		{
			this.gameObject.GetComponent<SpriteRenderer>().sprite = rollSprite;
		}

		//sticking = right trigger
		if (rightTriggerVal == 1 && touchingSurface)
		{
			//Debug.Log(this.groundContactPoint);
			this.GetComponent<Rigidbody2D>().gravityScale = 0;

			float gravityDirAmplifier = 4000f;

			float damp = 0.7f; 

			if ( Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0 )
			{

				if (Application.loadedLevelName == "test") {
					gravityDirAmplifier = 9000;
					damp = 1.0f;
				}

				if (Application.loadedLevelName == "duodenum"){
					gravityDirAmplifier = 14000f;				
				}

				if (Application.loadedLevelName == "stomach_sean") {
					gravityDirAmplifier = 12000;
				}

				if (Application.loadedLevelName == "nasal") {
					gravityDirAmplifier = 9000;
					damp = 1.0f; 
				}
			}

			GetComponent<Rigidbody2D> ().AddForce (gravityDir * gravityDirAmplifier);

			//GetComponent<Rigidbody2D>().AddForce ( -Physics2D.gravity );
			//playtests were done with gravityDir * 2000;

			ForceChar(horizMove * Input.GetAxis("Horizontal"), horizMove * 1.3f * Input.GetAxis ("Vertical") * 1.3f);

			float verticalSpeedCap = horizontalSpeedCap * 0.75f;

			//vertical velocity cap for moving
			if (Mathf.Abs (Input.GetAxis("Vertical")) > 0)
			{
				if (this.GetComponent<Rigidbody2D>().velocity.y > verticalSpeedCap)
				{
					this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, verticalSpeedCap);
				}
				if (this.GetComponent<Rigidbody2D>().velocity.y < -verticalSpeedCap)
				{
					this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -verticalSpeedCap);
				}
			}


			//GetComponent<Rigidbody2D>().AddForce ( gravityDir * gravityDirAmplifier );


		}

		if (rightTriggerVal < 1 || !touchingSurface)
		//if (Input.GetKeyUp(KeyCode.JoystickButton9) || !touchingSurface)
		{
			this.GetComponent<Rigidbody2D>().gravityScale = 3;
		}

		if(Mathf.Abs (Input.GetAxis("Horizontal")) > 0)
		{
			lastDirectionWhileKeypressed = GetComponent<Rigidbody2D>().velocity.x;
		}
	}

	void JoyStickRegularUpdate()
	{
		//splitting = TRIANGLE
		if (Input.GetKeyDown(split_joystick) && this.transform.localScale.x > 0.7f)
		{
			this.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			playerAudio.GetComponent<Player_sounds>().Play_SplitJoinAudio("split");
			isLarge = false;
			vertMove = 4500;
			GameObject clone = Instantiate (this.gameObject) as GameObject;
//			Debug.Log(clone);
		}

		//jump = X
		if (Input.GetKeyDown(jump_joystick) && (touchingGround /*|| this.touchingSurface*/))
		{
			//Debug.Log(jumpVec);
			Vector2 jumpVector = new Vector2(0,vertMove * 2);

			//jumping while sticking
			if (rightTriggerVal == 1)
			{
				//touching both left right
				if (touchingLeft && touchingRight)
				{
					GetComponent<Rigidbody2D>().AddForce(jumpVector);
				}

				//touching left
				if ((touchingLeft && !touchingRight) || (!touchingLeft && touchingRight))
				{
					GetComponent<Rigidbody2D>().AddForce(jumpVector * 0.65f);
				}
			}
			//not sticking
			else
			{
				GetComponent<Rigidbody2D>().AddForce(jumpVector);
			}
		}

		//horiz jump
		if (Input.GetKeyDown(jump_joystick) && touchingLeft && !touchingRight)
		{

			if (rightTriggerVal == 1)
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(horizMove * 16, 0));
			}
			else
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(horizMove * 7, 0));
			}
		}

		//horiz jump
		if (Input.GetKeyDown(jump_joystick) && touchingRight && !touchingLeft)
		{

			if (rightTriggerVal == 1)
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(-horizMove * 16, 0));
			}
			else
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(-horizMove * 7, 0));
			}

		}

		//horiz velocity cap for moving
		if (Input.GetAxis ("Horizontal") != 0)
		{
			if (this.GetComponent<Rigidbody2D>().velocity.x > horizontalSpeedCap)
			{
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalSpeedCap, this.GetComponent<Rigidbody2D>().velocity.y);
			}
			if (this.GetComponent<Rigidbody2D>().velocity.x < -horizontalSpeedCap)
			{
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(-horizontalSpeedCap, this.GetComponent<Rigidbody2D>().velocity.y);
			}
		}

	}
	
	void VelChar (float x, float y)
	{
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (x, y);
	}
	
	void ForceChar (float x, float y)
	{
		GetComponent<Rigidbody2D> ().AddForce (new Vector2 (x, y));
	}
	
	void KillCharacter ()
	{
		particleSys.transform.parent = this.transform.parent;
		particleSys.transform.gameObject.SetActive (true);

		playerAudio.transform.parent = this.transform.parent;
		playerAudio.GetComponent<Player_sounds>().Play_DeathAudio(this.transform.localScale.x);

		if (GameObject.FindGameObjectsWithTag("Player").Length > 1)
		{
			Destroy (this.gameObject);
		}
		else
		{
			StartCoroutine("LastWillDeath");
		}

	}

	IEnumerator LastWillDeath()
	{
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>().enabled = false;
		this.GetComponent<SpriteRenderer>().enabled = false;
		this.GetComponent<Collider2D>().enabled = false;

		foreach(SpriteRenderer sr in this.GetComponentsInChildren<SpriteRenderer>())
		{
			sr.enabled = false;
		}

		//this.GetComponentsInChildren<SpriteRenderer>()

		yield return new WaitForSeconds(1.3f);

		Application.LoadLevel (Application.loadedLevel);
	}
	
	void OnCollisionEnter2D (Collision2D col)
	{
		
		if (col.gameObject.tag == "Ground") {
			touchingSurface = true;

			playerAudio.GetComponent<Player_sounds>().Play_LandingAudio();
			
			//Debug.Log(col.contacts.Length);
			
			foreach (ContactPoint2D contactPoint in col.contacts) {
				//print (	contactPoint.point);
				
				groundContactPoint = contactPoint.point;
			}
		}
		
		//		if (col.gameObject.tag == "Ground")
		//		{
		//			this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
		//		}
		
		if (col.gameObject.tag == "Food" && this.transform.localScale.x < 1.0f) {
			this.transform.localScale = new Vector3 (1f, 1f, 1f);
			isLarge = true;
			vertMove = 6000;
			Destroy (col.gameObject);
		}
		
		if (col.gameObject.tag == "Enemy") {

			KillCharacter ();
			
		}
	}
	
	void OnCollisionStay2D (Collision2D col)
	{
		if (col.gameObject.tag == "Ground") {
			touchingSurface = true;
			
			//Debug.Log(col.contacts.Length);
			
			foreach (ContactPoint2D contactPoint in col.contacts) {
				//print (	contactPoint.point);
				
				groundContactPoint = contactPoint.point;
			}
		}
	}
	
	void OnCollisionExit2D (Collision2D col)
	{
		if (col.gameObject.tag == "Ground") {
			touchingSurface = false;
		}
	}
	
	void OnTriggerEnter2D (Collider2D col)
	{
		
		if (col.gameObject.tag == "Enemy") {

			KillCharacter ();
			
		}
	} 
	/*
    void OnTriggerStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            touchingGround = true;
        }
    }

    void OnTriggerExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            touchingGround = false;
        }
    }*/
	
	//THIS IS A TEMPORARY FIX FOR CHECKPOINT PLACE ISSUE
	void OnApplicationQuit ()
	{
		print ("quitting application");
		PlayerPrefs.SetInt ("PlayCount", 0);
		PlayerPrefs.SetFloat ("lastX", 0.8f);
		PlayerPrefs.SetFloat ("lastY", -20.5f);
	}
	
}
