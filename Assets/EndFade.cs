﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndFade : MonoBehaviour {

	public GameObject img;

	public float alpha = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		alpha += 0.003f;

		img.GetComponent<Image>().color = new Color(0, 0, 0, alpha);

		if (alpha > 1.2f)
		{
			Application.LoadLevel("end");
		}
	
	}
}
