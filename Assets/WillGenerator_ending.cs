﻿using UnityEngine;
using System.Collections;

public class WillGenerator_ending : MonoBehaviour {

	public GameObject will_cutscene;

	public float scaleMin, scaleMax;

	public string dir;

	private float counter;
	public float counter_interval;

	public int generatorTimer = 0;
	public int generatorStart, generatorEnd;
	
	public float xMin, xMax, yMin, yMax;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		generatorTimer ++;

		if (generatorTimer > generatorStart)
		{
			counter += Random.Range (0.7f, 1.3f);
			if (counter > counter_interval)
			{
				GameObject tempWill = Instantiate (will_cutscene) as GameObject;
				tempWill.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
				tempWill.transform.parent = this.transform;

				float scaleVal = Random.Range (scaleMin, scaleMax);
				tempWill.transform.localScale = new Vector2(scaleVal, scaleVal);

				tempWill.GetComponent<Rigidbody2D>().velocity = new Vector2( Random.Range (xMin, xMax), Random.Range (yMin, yMax) );
				counter = 0;
			}

			if (generatorTimer > generatorEnd)
			{
				GameObject.Destroy(this.gameObject);
			}
		}
	
	}

	IEnumerator GenerateWills() {
		return null;
	}
}
